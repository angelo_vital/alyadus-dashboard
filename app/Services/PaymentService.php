<?php


namespace App\Services;


use App\Plan;
use App\Provider;
use App\Services\Zoop\ZoopService;

class PaymentService
{

    public function processSubscription($providerId, $planId, $boleto = false)
    {
        $provider = Provider::find($providerId);
        $plan = Plan::find($planId);
        $data = [];

        $data['amount'] = intval($plan->price);
        $data['currency'] = 'BRL';
        $data['description'] = 'Pagamento de mensalidade';
        $data['on_behalf_of'] = '9236f50a1889488789a7c3e891766f50';
        $data['source'] = ['usage' => 'single_use'];
        $data['source'] = ['currency' => 'BRL'];
        $data['source'] = ['type' => 'card'];
        $data['source'] = ['card' => ['id' => $provider->card()->get()->last()->card_id]];
        $data['source'] = ['customer' => $provider->seller_id];

        if($boleto){
            $data['payment_type'] = 'boleto';
        } else {
            $data['payment_type'] = 'credit';
        }

        $response = (new ZoopService())->createTransaction($data);




    }

}