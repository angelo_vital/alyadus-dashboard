<?php


namespace App\Services\Zoop;


use App\Exceptions\BoletoErrorException;
use App\Exceptions\BuyerCreateException;
use App\Exceptions\ZoopCardAddException;
use App\Exceptions\ZoopFailedPayment;
use App\Http\Controllers\ProviderResources\TripController;
use App\Http\Controllers\SendPushNotification;
use App\PaymentLog;
use App\Provider;
use App\User;
use App\UserRequestPayment;
use App\UserRequests;
use Curl\Curl;
use Curl\MultiCurl;
use Illuminate\Support\Facades\Log;
use CURLFile;


class ZoopService
{

    private $client;
    private $multiCurl;

    public function __construct()
    {

        $this->client = new Curl('https://api.zoop.ws/');
        $this->client->setBasicAuthentication('zpk_prod_CdKYtCydiw3ToZ0Q8AUJNyMj', '');
        $this->client->setHeader('Content-Type', 'application/json');

        $this->multiCurl = new MultiCurl('https://api.zoop.ws/');
        $this->multiCurl->setBasicAuthentication('zpk_prod_CdKYtCydiw3ToZ0Q8AUJNyMj', '');
        $this->multiCurl->setHeader('Content-Type', 'application/json');

    }

    public function addCard(array $data)
    {

        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/cards/tokens', $data);

        if($this->client->error){
            throw new ZoopCardAddException($this->client->response->error->message);
        } else {
            return [
                'id' => $this->client->response->card->id,
                'token' => $this->client->response->id,
                'brand' => $this->client->response->card->card_brand,
                'last_four' => $this->client->response->card->first4_digits
            ];
        }
    }

    public function attachCardToUser($cardToken, $customer)
    {
        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/cards', [
            'token' => $cardToken,
            'customer' => $customer
        ]);

        if ($this->client->error) {
            Log::error('Error: '. $this->client->response->error->message);
        }
    }

    public function sendSellerDocuments($sellerId, $fileUrl, $type)
    {
        $this->client->setHeader('Content-Type', 'multipart/form-data');
        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/sellers/'.$sellerId.'/documents', [
            'file' => new CURLFile(storage_path('app/public/'.$fileUrl)),
            'category' => $type
        ]);

    }

    public function createTransaction($random, $customer, $amount, $card)
    {
        $transaction = [];

        $transaction['on_behalf_of'] = '9236f50a1889488789a7c3e891766f50';
        $transaction['customer'] = $customer;
        $transaction['amount'] = $amount;
        $transaction['currency'] = 'BRL';
        $transaction['description'] = 'Pagamento de viagem';
        $transaction['payment_type'] = 'credit';
        $transaction['capture'] = false;
        $transaction['source'] = [
            'usage' => 'single_use',
            'currency' => 'BRL',
            'amount' => $amount,
            'type' => 'card',
            'card' => ['id' => $card],
        ];

        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/transactions', $transaction);

        if($this->client->error){
            throw new ZoopFailedPayment($this->client->response->error->message);
        } else {
            return [
                'success' => true,
                'transaction_id' => $this->client->response->id,
            ];
        }
    }

    public function captureTransaction($random, $transaction, $amount)
    {
        $transactionData = [
            'on_behalf_of' => '9236f50a1889488789a7c3e891766f50',
            'amount' => $amount
        ];

        $this->client->post("v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/transactions/{$transaction}/capture", $transactionData);

        if($this->client->error){
            $this->failure($random);
            return false;
        } else {
            return $this->response($random, $this->client->response->id);
        }
    }

    public function createSubscriptionTransaction($random, $customer, $amout, $card)
    {

        $transaction = [];

        $transaction['on_behalf_of'] = '9236f50a1889488789a7c3e891766f50';
        $transaction['customer'] = $customer;
        $transaction['amount'] = $amout;
        $transaction['currency'] = 'BRL';
        $transaction['description'] = 'Pagamento de mensalidade';
        $transaction['payment_type'] = 'credit';
        $transaction['capture'] = false;
        $transaction['source'] = [
            'usage' => 'single_use',
            'currency' => 'BRL',
            'amount' => $amout,
            'type' => 'card',
            'card' => ['id' => $card],
        ];

        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/transactions', $transaction);

        if($this->client->error){
            throw new ZoopFailedPayment($this->client->response->error->message);
        } else {
            return [
                'success' => true,
                'transaction_id' => $this->client->response->id,
            ];
        }

    }

    public function createBoletoTransaction(array $data)
    {

        $transaction = [];

        $transaction['on_behalf_of'] = '9236f50a1889488789a7c3e891766f50';
        $transaction['customer'] = $data['customer'];
        $transaction['amount'] = $data['amount'];
        $transaction['currency'] = 'BRL';
        $transaction['description'] = 'Pagamento de mensalidade';
        $transaction['payment_type'] = 'boleto';

        $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/transactions', $transaction);

        if($this->client->error){
            throw new BoletoErrorException($this->client->response->error->message);
        } else {
            return [
                'success' => true,
                'transaction_id' => $this->client->response->id,
                'boleto_id' => $this->client->response->payment_method->id,
                'url' => $this->client->response->payment_method->url
            ];
        }

    }

    public function regenerateBoleto(array $data)
    {

        $this->client->get('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/transactions/'.$data['transaction_id']);

        if($this->client->error){
            throw new BoletoErrorException($this->client->response->error->message);
        } else {
            return [
                'success' => true,
                'url' => $this->client->response->payment_method->url
            ];
        }

    }

    public function listSellers()
    {

        try{

            $this->client->get('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/sellers');
            $response = $this->client->getResponse();

            dd($response->items);
//            return $response->items;

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function listBuyers()
    {

        try{

            $this->client->get('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/buyers');
            $response = $this->client->getResponse();
            dd($response);
//            foreach ($response->items as $item) {
//                $this->deleteBuyer($item->id);
//                dump($item->id);
//            }
//            return $response->items;

        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function createBuyer(array $data)
    {

        $buyerData = $this->prepareBuyerData($data);

        $this->client->post('/v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/buyers', $buyerData);

        if($this->client->error){
            throw new BuyerCreateException($this->client->response->error->message);
        } else {
            return [
                'success' => true,
                'buyer_id' => $this->client->response->id
            ];
        }
    }

    public function createSeller(array $data)
    {
        try{

            $sellerData = $this->prepareSellerData($data);

            $this->client->post('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/sellers/individuals', $sellerData);

            if($this->client->error){
                Log::error($this->client->getErrorMessage());
            } else {
                return $this->client->response->id;
            }

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    public function deleteBuyer($id)
    {
        $this->client->delete("/v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/buyers/{$id}");
        $response = $this->client->getResponse();

        dump($response);
    }

    public function deleteSeller()
    {
        $this->client->delete('v1/marketplaces/d4484f0319fe4eaf983e66a116b634f3/sellers/7ce1d33f67ba45538c44ab86fce2ba98');

        if($this->client->error){
            Log::error($this->client->getErrorMessage());
        } else {
            dd($this->client->response);
        }
    }

    private function prepareBuyerData(array $data):array
    {

        return [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'taxpayer_id' => $data['cpf'],
            'email' => $data['email'],
            'phone_number' => $data['mobile'],
            'birthdate' => $data['birthday'],
            'address' => [
                'line1' => $data['street'],
                'line2' => $data['number'],
                'line3' => $data['complement'],
                'neighborhood' => $data['neighborhood'],
                'city' => $data['city'],
                'state' => $data['state'],
                'postal_code' => $data['cep'],
                'country_code' => 'BR'
            ],
        ];

    }

    private function prepareSellerData(array $data):array
    {
        return [
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'taxpayer_id' => $data['cpf'],
            'email' => $data['email'],
            'phone_number' => $data['mobile'],
            'birthdate' => $data['birthday'],
            'address' => [
                'line1' => $data['street'],
                'line2' => $data['number'],
                'line3' => $data['complement'],
                'neighborhood' => $data['neighborhood'],
                'city' => $data['city'],
                'state' => $data['state'],
                'postal_code' => $data['cep'],
                'country_code' => 'BR'
            ],
            'mcc' => '46'
        ];
    }

    private function prepareCardData(array $data): array
    {

        return [
            'holder_name' => $data['holder_name'],
            'expiration_month' => $data['expiration_month'],
            'expiration_year' => $data['expiration_year'],
            'card_number' => $data['card_number'],
            'security_code' => $data['security_code'],
        ];

    }


    /**
     *
     * MÉTODOS PRIVADOS
     *
     */

    private function response($order, $paymentId)
    {

        $log = PaymentLog::where('transaction_code', $order)->first();

        if ($log->is_wallet == 1) {

            if ($log->user_type == "user") {

                $user = User::find($log->user_id);
                (new TripController)->userCreditDebit($log->amount, $user->id, 1);
                (new SendPushNotification)->WalletMoney($user->id, currency($log->amount));

            } else if ($log->user_type == "provider") {

                $user = Provider::find($log->user_id);
                (new TripController)->providerCreditDebit($log->amount, $user->id, 1);
                (new SendPushNotification)->ProviderWalletMoney($user->id, currency($log->amount));

            }

            $wallet_balance = $user->wallet_balance + $log->amount;

            if ($log->user_type == "provider" && $wallet_balance > (float)config('constants.minimum_negative_balance')) {
                Provider::where('id', $log->user_id)->update(['status' => 'approved']);
            }

            return [
                'success' => true,
                'message' => currency($log->amount) . " " . trans('api.added_to_your_wallet'),
                'wallet_balance' => $wallet_balance
            ];

        } else {

            $payment_id = $paymentId;

            if ($log->payment_mode == "CARD") {
                if (!is_null($log->transaction_id)) {
                    $RequestPayment = UserRequestPayment::where('request_id', $log->transaction_id)->first();
//                    (new SendPushNotification)->sendPushToProvider($RequestPayment->provider_id, "Você recebeu R$". $RequestPayment->provider_pay . " na sua carteira!" );
                    (new SendPushNotification)->captureAutorization($RequestPayment->user_id, "Debitamos do seu cartão o valor de R$" . $RequestPayment->total . "!");
                }
            }

            $userRequest = UserRequests::find($log->transaction_id);

            if ($userRequest) {
                $RequestPayment = UserRequestPayment::where('request_id', $userRequest->id)->first();
                $RequestPayment->payment_id = $payment_id;
                $RequestPayment->payment_mode = $userRequest->payment_mode;
                $RequestPayment->card = $RequestPayment->payable;
                $RequestPayment->save();

                $userRequest->paid = 1;
                $userRequest->status = 'COMPLETED';
                $userRequest->save();

                //for create the transaction
                (new TripController)->callTransaction($userRequest->id);
            }

            return [
                'success' => true,
                'message' => trans('api.paid')
            ];
        }

    }

    private function failure($order)
    {

        $log = PaymentLog::where('transaction_code', $order)->first();

        if ($log->is_wallet == 1) {

            throw new ZoopFailedPayment("Erro ao processar pagamento");

        }

        throw new ZoopFailedPayment("Erro ao processar pagamento");

    }

}