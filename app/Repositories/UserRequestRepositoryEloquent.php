<?php

namespace App\Repositories;

use App\UserRequests;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\UserRequestRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRequestRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRequestRepositoryEloquent extends BaseRepository implements UserRequestRepository
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRequests::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
