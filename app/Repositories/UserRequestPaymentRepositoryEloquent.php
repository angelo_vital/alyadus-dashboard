<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\UserRequestPaymentRepository;
use App\UserRequestPayment;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRequestPaymentRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class UserRequestPaymentRepositoryEloquent extends BaseRepository implements UserRequestPaymentRepository
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRequestPayment::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
