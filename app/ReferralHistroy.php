<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ReferralHistroy
 * @mixin \Eloquent
 */
class ReferralHistroy extends Model
{
    protected $table='referral_histroy';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
    protected $fillable = [
        'referrer_id',        
        'type',        
        'referral_id',
        'referral_data', 
        'status',       
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public function indicator()
    {
        return $this->belongsTo(Provider::class, 'referrer_id', 'id');
    }

    public function indicated()
    {
        return $this->belongsTo(Provider::class, 'referral_id', 'id');
    }

    public function subscription()
    {
        return $this->hasMany(ProviderPlan::class, 'referral_id', 'id');
    }

}
