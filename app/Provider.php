<?php

namespace App;

use App\Notifications\ProviderResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use NotificationChannels\WebPush\HasPushSubscriptions;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Provider
 * @mixin \Eloquent
 */
class Provider extends Authenticatable
{
    use HasApiTokens,Notifiable,HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id',
        'plan_id',
        'plan_paid',
        'seller_id',
        'first_name',
        'last_name',
        'cpf',
        'street',
        'number',
        'complement',
        'neighborhood',
        'city',
        'state',
        'cep',
        'birthday',
        'email',
        'password',
        'mobile',
        'address',
        'picture',
        'gender',
        'latitude',
        'longitude',
        'status',
        'avatar',
        'gender',
        'social_unique_id',
        'fleet',
        'login_by',
        'paypal_email',
        'referral_unique_id',
        'qrcode_url',
        'country_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function referrer()
    {
        return $this->hasMany(ReferralHistroy::class, 'referrer_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(ProviderPaymentMode::class);
    }

    public function service()
    {
        return $this->hasOne('App\ProviderService');
    }

    public function banks()
    {
        return $this->hasMany('App\ProviderBank', 'provider_id', 'id');
    }

    public function subscription()
    {
        return $this->hasMany(ProviderPlan::class, 'provider_id', 'id');
    }

    public function card()
    {
        return $this->hasMany(ProviderCard::class, 'user_id', 'id');
    }

    public function incoming_requests()
    {
        return $this->hasMany('App\RequestFilter')->where('status', 0);
    }

    public function requests()
    {
        return $this->hasMany('App\RequestFilter');
    }

    public function profile()
    {
        return $this->hasOne('App\ProviderProfile');
    }

    public function device()
    {
        return $this->hasOne('App\ProviderDevice');
    }

    public function trips()
    {
        return $this->hasMany('App\UserRequests');
    }

    public function accepted()
    {
        return $this->hasMany('App\UserRequests','provider_id')
                    ->where('status','!=','CANCELLED');
    }

    public function cancelled()
    {
        return $this->hasMany('App\UserRequests','provider_id')
                ->where('status','CANCELLED');
    }

    /**
     * The services that belong to the user.
     */
    public function documents()
    {
        return $this->hasMany('App\ProviderDocument');
    }

    /**
     * The services that belong to the user.
     */
    public function document($id)
    {
        return $this->hasOne('App\ProviderDocument')->where('document_id', $id)->first();
    }

    /**
     * The services that belong to the user.
     */
    public function pending_documents()
    {
        return $this->hasMany('App\ProviderDocument')->where('status', 'ASSESSING')->count();
    }

    public function active_documents()
    {
        return $this->hasMany('App\ProviderDocument')->where('status', 'ACTIVE')->count();
    }

    public function total_requests()
    {
        return $this->hasMany('App\UserRequests','provider_id')->count();
    }

    public function accepted_requests()
    {
        return $this->hasMany('App\UserRequests','provider_id')->where('status','!=','CANCELLED')->count();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ProviderResetPassword($token));
    }

    public function getFirstNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getLastNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}
