<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProviderBank
 * @mixin \Eloquent
 */
class ProviderBank extends Model
{
    protected $fillable = [
        'provider_id', 'bank_id','agency','account_number','type'
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id', 'id');
    }

    public function getInstitutionAttribute($value)
    {
        return strtoupper($value);
    }

    public function getTypeAttribute($value)
    {
        if($value == "CC"){
            return "Conta Corrente";
        } else {
            return "Conta Poupança";
        }
    }
}
