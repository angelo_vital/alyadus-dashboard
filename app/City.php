<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class City
 * @mixin \Eloquent
 */
class City extends Model
{
    protected $fillable = ['title', 'state_id','lat','longi','iso'];
}
