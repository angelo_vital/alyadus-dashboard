<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRequestRoutes
 * @mixin \Eloquent
 */
class UserRequestRoutes extends Model
{
    protected $fillable = ['request_id','lat','lng','bearing'];

    public function request()
    {
        return $this->belongsTo(UserRequests::class);
    }
}
