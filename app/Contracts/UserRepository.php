<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\CacheableInterface;

/**
 * Interface UserRepository.
 *
 * @package namespace App\Contracts;
 */
interface UserRepository extends CacheableInterface
{
    //
}
