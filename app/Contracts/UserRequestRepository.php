<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\CacheableInterface;

/**
 * Interface UserRequestRepository.
 *
 * @package namespace App\Contracts;
 */
interface UserRequestRepository extends CacheableInterface
{
    //
}
