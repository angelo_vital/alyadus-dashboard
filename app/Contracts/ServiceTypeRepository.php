<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\CacheableInterface;

/**
 * Interface ServiceTypeRepository.
 *
 * @package namespace App\Contracts;
 */
interface ServiceTypeRepository extends CacheableInterface
{
    //
}
