<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\CacheableInterface;

/**
 * Interface ProviderRepository.
 *
 * @package namespace App\Contracts;
 */
interface ProviderRepository extends CacheableInterface
{
    //
}
