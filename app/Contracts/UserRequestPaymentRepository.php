<?php

namespace App\Contracts;

use Prettus\Repository\Contracts\CacheableInterface;

/**
 * Interface UserRequestPaymentRepository.
 *
 * @package namespace App\Contracts;
 */
interface UserRequestPaymentRepository extends CacheableInterface
{
    //
}
