<?php

namespace App\Jobs;

use App\Provider;
use App\Services\Zoop\ZoopService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SellerDocsSend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $provider;

    /**
     * SendProviderDocuments constructor.
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if($this->provider->pending_documents() == 0){

            $docs = $this->provider->documents()->where('status','ACTIVE')->get();
            $sended = false;
            $data = [];

            foreach ($docs as $doc) {

                if($doc->document_id == 1){
                    $data[] = [
                        'sellerId' => $this->provider->seller_id,
                        'url' => $doc->url,
                        'type' => 'identificacao'
                    ];
                } else if($doc->document_id == 3){
                    $data[] = [
                        'sellerId' => $this->provider->seller_id,
                        'url' => $doc->url,
                        'type' => 'residencia'
                    ];
                } else if(!$sended){
                    $data[] = [
                        'sellerId' => $this->provider->seller_id,
                        'url' => 'atividade.pdf',
                        'type' => 'atividade'
                    ];

                    $sended = true;
                }

            }

            (new ZoopService())->sendSellerDocuments($data);

        }
    }
}
