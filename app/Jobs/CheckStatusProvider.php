<?php

namespace App\Jobs;

use App\Provider;
use App\ProviderService;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class CheckStatusProvider implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $Providers = Provider::with('service')
            ->whereHas('service', function($query){
                $query->where('status','hold');
                $query->where('updated_at','<=', Carbon::now()->subHour(2));
            })
            ->get();

        if(!empty($Providers)){
            foreach($Providers as $Provider){
                DB::table('provider_services')->where('provider_id', $Provider->id)->update(['status' => 'offline']);
            }
        }
    }
}
