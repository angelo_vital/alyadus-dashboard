<?php

namespace App\Jobs;

use App\CustomPush;
use App\Provider;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SendPushNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 0;

    private $push;

    /**
     * Create a new job instance.
     *
     * @param CustomPush $push
     */
    public function __construct(CustomPush $push)
    {
        $this->push = $push;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

//            $this->push = CustomPush::findOrFail($this->pushId);
            $cityId = $this->push->city_id;
            Log::info("City: ". $cityId);
            Log::info("Condition: ". $this->push->condition);

            if ($this->push->send_to == 'USERS') {

                $Users = [];

                if ($this->push->condition == 'ACTIVE') {

                    if ($this->push->condition_data == 'HOUR') {

                        $Users = User::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subHour());
                        })->where("city_id", $cityId)
                            ->get();

                    } elseif ($this->push->condition_data == 'WEEK') {

                        $Users = User::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subWeek());
                        })->where("city_id", $cityId)
                            ->get();

                    } elseif ($this->push->condition_data == 'MONTH') {

                        $Users = User::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subMonth());
                        })->where("city_id", $cityId)
                            ->get();

                        Log::info("TOTAL USER: ". $Users->count());
                    }
                } elseif ($this->push->condition == 'RIDES') {

                    $push = $this->push;

                    $Users = User::whereHas('trips', function($query) use ($push) {
                        $query->where('status', 'COMPLETED');
                        $query->groupBy('id');
                        $query->havingRaw('COUNT(*) >= ' . $push->condition_data);
                    })->where("city_id", $cityId)
                        ->get();
                } elseif ($this->push->condition == 'LOCATION') {

                    $Location = explode(',', $this->push->condition_data);

                    $distance = config('constants.provider_search_radius', '10');
                    $latitude = $Location[0];
                    $longitude = $Location[1];

                    $Users = User::whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                        ->where("city_id", $cityId)
                        ->get();

                } elseif ($this->push->condition == 'ALL') {
                    $Users = User::where("city_id", $cityId)->get();

                    Log::info("TOTAL USER: ". $Users->count());
                }


                foreach ($Users as $key => $user) {
                    (new \App\Http\Controllers\SendPushNotification())->sendPushToUser($user->id, $this->push->message);
                }
            } elseif ($this->push->send_to == 'PROVIDERS') {


                $Providers = [];

                if ($this->push->condition == 'ACTIVE') {

                    if ($this->push->condition_data == 'HOUR') {

                        $Providers = Provider::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subHour());
                        })->where("city_id", $cityId)
                            ->get();
                    } elseif ($this->push->condition_data == 'WEEK') {

                        $Providers = Provider::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subWeek());
                        })->where("city_id", $cityId)
                            ->get();
                    } elseif ($this->push->condition_data == 'MONTH') {

                        $Providers = Provider::whereHas('trips', function($query) {
                            $query->where('created_at', '>=', Carbon::now()->subMonth());
                        })->where("city_id", $cityId)
                            ->get();
                    }
                } elseif ($this->push->condition == 'RIDES') {

                    $push = $this->push;

                    $Providers = Provider::whereHas('trips', function($query) use ($push) {
                        $query->where('status', 'COMPLETED');
                        $query->groupBy('id');
                        $query->havingRaw('COUNT(*) >= ' . $push->condition_data);
                    })->where("city_id", $cityId)
                        ->get();
                } elseif ($this->push->condition == 'LOCATION') {

                    $Location = explode(',', $this->push->condition_data);

                    $distance = config('constants.provider_search_radius', '10');
                    $latitude = $Location[0];
                    $longitude = $Location[1];

                    $Providers = Provider::whereRaw("(6371 * acos( cos( radians('$latitude') ) * cos( radians(latitude) ) * cos( radians(longitude) - radians('$longitude') ) + sin( radians('$latitude') ) * sin( radians(latitude) ) ) ) <= $distance")
                        ->where("city_id", $cityId)
                        ->get();
                } elseif($this->push->condition == 'ALL'){

                    $Providers = Provider::where("city_id", $cityId)->get();

                    Log::info("TOTAL PROVIDER: ". $Providers->count());
                }


                foreach ($Providers as $key => $provider) {
                    (new \App\Http\Controllers\SendPushNotification())->sendPushToProvider($provider->id, $this->push->message);
                }
            } elseif ($this->push->send_to == 'ALL') {

                $Users = User::all();
                foreach ($Users as $key => $user) {
                    (new \App\Http\Controllers\SendPushNotification())->sendPushToUser($user->id, $this->push->message);
                }

                $Providers = Provider::all();
                foreach ($Providers as $key => $provider) {
                    (new \App\Http\Controllers\SendPushNotification())->sendPushToProvider($provider->id, $this->push->message);
                }
            }
        } catch (Exception $e) {
            Log::alert('Algo deu errado!');
        }
    }
}
