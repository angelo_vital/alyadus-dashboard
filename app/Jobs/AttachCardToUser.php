<?php

namespace App\Jobs;

use App\Services\Zoop\ZoopService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AttachCardToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $token;
    private $customer;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($token, $customer)
    {
        $this->token = $token;
        $this->customer = $customer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->token != ""){
            (new ZoopService())->attachCardToUser($this->token, $this->customer);
        }
    }
}
