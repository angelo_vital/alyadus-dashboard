<?php

namespace App\Jobs;

use App\Provider;
use App\Services\Zoop\ZoopService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SellerIdZoop implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $provider;

    /**
     * SellerIdZoop constructor.
     * @param Provider $provider
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(is_null($this->provider->seller_id)){
            $resData = (new ZoopService())->createSeller($this->provider->toArray());
            $this->provider->update(['seller_id' => $resData]);
        }
    }
}
