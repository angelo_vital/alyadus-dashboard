<?php

namespace App\Jobs;

use App\Http\Controllers\SendPushNotification;
use App\UserRequests;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendStatusNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $type;
    private $userRequest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRequests $userRequest, $type)
    {
        $this->userRequest = $userRequest;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        switch ($this->type) {
            case "complete":
                (new SendPushNotification)->Complete($this->userRequest);
                break;
            case "arrived":
                (new SendPushNotification)->Arrived($this->userRequest);
                break;
            case "pickedup":
                (new SendPushNotification)->Pickedup($this->userRequest);
                break;
            case "dropped":
                (new SendPushNotification)->Dropped($this->userRequest);
                break;
            case "accept":
                (new SendPushNotification)->RideAccepted($this->userRequest);
                break;
        }
    }
}
