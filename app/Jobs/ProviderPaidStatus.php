<?php

namespace App\Jobs;

use App\Provider;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProviderPaidStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $provider;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $providerId = $this->provider->id;
        $this->provider->update(['status' => 'waiting']);

        (new \App\Http\Controllers\SendPushNotification())->ApproveProvider($providerId);
    }
}
