<?php

namespace App\Jobs;

use App\Helpers\Helper;
use App\UserRequests;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendInvoiceTrip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 0;
    private $userRequest;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserRequests $userRequest)
    {
        $this->userRequest = $userRequest;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Helper::site_sendmail($this->userRequest);
    }
}
