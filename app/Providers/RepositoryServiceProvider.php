<?php

namespace App\Providers;

use App\Contracts\ProviderRepository;
use App\Contracts\ServiceTypeRepository;
use App\Contracts\UserRepository;
use App\Contracts\UserRequestPaymentRepository;
use App\Contracts\UserRequestRepository;
use App\Repositories\ProviderRepositoryEloquent;
use App\Repositories\ServiceTypeRepositoryEloquent;
use App\Repositories\UserRepositoryEloquent;
use App\Repositories\UserRequestPaymentRepositoryEloquent;
use App\Repositories\UserRequestRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(UserRequestRepository::class, UserRequestRepositoryEloquent::class);
        $this->app->bind(UserRequestPaymentRepository::class, UserRequestPaymentRepositoryEloquent::class);
        $this->app->bind(ServiceTypeRepository::class, ServiceTypeRepositoryEloquent::class);
        $this->app->bind(ProviderRepository::class, ProviderRepositoryEloquent::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
