<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Bank
 * @mixin \Eloquent
 */
class Bank extends Model
{
    protected $fillable = ['code', 'name'];
}
