<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProviderPlan
 * @mixin \Eloquent
 */
class ProviderPlan extends Model
{
    protected $fillable = ['provider_id', 'plan_id','transaction_id','boleto_id','type','reference','started_at','finished_at','status'];

    public function getStatusAttribute($value)
    {
        return $value == 1? "Pago": "Não pago";
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class, "plan_id", "id");
    }
}
