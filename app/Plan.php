<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan
 * @mixin \Eloquent
 */
class Plan extends Model
{
    protected $fillable = ['name','price','period'];
}
