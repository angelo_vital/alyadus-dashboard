<?php

namespace App\Console;

use App\Jobs\CheckPaymentProvider;
use App\Jobs\CheckStatusProvider;
use App\Jobs\ProvidersUpdateFleet;
use App\Jobs\UpdateRequestCity;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CustomCommand::class,
        \App\Console\Commands\DbClearCommand::class,
        \App\Console\Commands\ProviderToHold::class,
        \App\Console\Commands\CheckPayment::class,
        \App\Console\Commands\CheckIndicatedPayment::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cronjob:rides')
            ->timezone('America/Porto_Velho')
            ->everyMinute();

        $schedule->command('cronjob:providers')
            ->everyFiveMinutes();

        $schedule->command('cronjob:payment')
            ->dailyAt('00:00');

        $schedule->command('cronjob:indicated')
            ->dailyAt('00:00');

        $schedule->job(new CheckStatusProvider())
            ->everyFiveMinutes();

        $schedule->job(new ProvidersUpdateFleet())
            ->everyTenMinutes();

//        $schedule->call('App\Http\Controllers\AdminController@DBbackUp')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
