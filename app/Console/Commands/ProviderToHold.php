<?php

namespace App\Console\Commands;

use App\Provider;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ProviderToHold extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:providers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza status do motorista caso esteja ausente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Providers = Provider::with('service')
            ->whereHas('service', function ($query) {
                $query->where('status', 'active');
                $query->where('updated_at', '<=', Carbon::now()->subMinute(10));
            })
            ->get();

        if (!empty($Providers)) {
            foreach ($Providers as $Provider) {
                DB::table('provider_services')->where('provider_id',$Provider->id)->update(['status' => 'hold']);
            }
        }
    }
}
