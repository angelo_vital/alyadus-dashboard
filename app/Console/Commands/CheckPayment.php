<?php

namespace App\Console\Commands;

use App\Provider;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:payment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica pagamento dos motoristas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $providers = Provider::whereHas('subscription', function ($query) {
            $query->where('status', 1);
            $query->whereDate('started_at', '<=', Carbon::now()->format('Y/m/d'));
            $query->whereDate('finished_at', '>=', Carbon::now()->format('Y/m/d'));
        })
            ->get();

        $providersPaid = array();

        foreach ($providers as $provider) {
            $providersPaid[] = $provider->id;
        }

        $drivers = Provider::whereNotIn('id', $providersPaid)
            ->where('status', 'approved')
            ->get();

        foreach ($drivers as $driver) {
            Provider::find($driver->id)->update(['status' => 'balance']);
            (new \App\Http\Controllers\SendPushNotification())->DisapproveProvider($driver->id);
        }
    }
}
