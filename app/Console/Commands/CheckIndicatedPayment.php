<?php

namespace App\Console\Commands;

use App\ProviderPlan;
use App\ReferralHistroy;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckIndicatedPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cronjob:indicated';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica pagamento dos indicados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = ProviderPlan::where('status', 1)
            ->whereDate('started_at', '<=', Carbon::now()->format('Y/m/d'))
            ->whereDate('finished_at', '>=', Carbon::now()->format('Y/m/d'))
            ->get();

        $indicated = array();

        foreach ($subscriptions as $subscription) {
            $indicated[] = $subscription->provider_id;
        }

        ReferralHistroy::whereNotIn('referral_id',$indicated)->update(['status' => 'C']);
    }
}
