<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendSubscriptionMail extends Notification implements ShouldQueue
{
    use Queueable;

    private $boletoUrl;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($boletoUrl)
    {
        $this->boletoUrl = $boletoUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Ativação de licença UIU")
            ->line('Segue o link com o boleto referente a sua mensalidade, qualquer dúvida entre em contato conosco')
            ->action('Visualizar Boleto', $this->boletoUrl)
            ->line('Nossos agradecimentos');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
