<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Card
 * @mixin \Eloquent
 */
class Card extends Model
{
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
