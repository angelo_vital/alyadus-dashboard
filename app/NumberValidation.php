<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NumberValidation
 * @mixin \Eloquent
 */
class NumberValidation extends Model
{
    protected $fillable = [
      'code', 'mobile', 'otp'
    ];
}
