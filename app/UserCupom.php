<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCupom
 * @mixin \Eloquent
 */
class UserCupom extends Model
{
    protected $fillable = [
        'user_id','code'
    ];
}
