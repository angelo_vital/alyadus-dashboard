<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Settings
 * @mixin \Eloquent
 */
class Settings extends Model
{
    public $timestamps = false;
}
