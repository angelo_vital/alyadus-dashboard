<?php

namespace App\Http\Controllers;

use App\User;
use App\Provider;
use App\ProviderDevice;
use Exception;
use App;
use PushNotification;

class SendPushNotification extends Controller {

    /**
     * Notificação de viagem aceita pelo motorista
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function RideAccepted($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);
        return $this->sendPushToUser($request->user_id, trans('api.push.request_accepted'));
    }

    /**
     * Notificação de Viagem agendada pelo passageiro
     *
     * @param $user
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function user_schedule($user) {
        $user = User::where('id', $user)->first();
        $language = $user->language;
        App::setLocale($language);
        return $this->sendPushToUser($user, trans('api.push.schedule_start'));
    }

    /**
     * Notificação de agendamento para o motorista
     *
     * @param $provider
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function provider_schedule($provider) {

        $provider = Provider::where('id', $provider)->with('profile')->first();
        if ($provider->profile) {
            $language = $provider->profile->language;
            App::setLocale($language);
        }

        return $this->sendPushToProvider($provider, trans('api.push.schedule_start'));
    }

    /**
     * Notificação de cancelamento relizada pelo passageiro
     *
     * @param $request
     * @param $message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function UserCancellRide($request) {

        if($request->provider_id){
            $provider = Provider::where('id', $request->provider_id)->with('profile')->first();

            if ($provider->profile) {
                $language = $provider->profile->language;
                App::setLocale($language);
            }

            $message = PushNotification::Message(trans('api.push.user_cancelled'), [
                'badge' => 1,
                'sound' => 'default',
                'custom' => ['type' => 'passCancelTrip']
            ]);

            return $this->sendPushToProvider($request->provider_id, $message);
        }

    }

    public function ApproveProvider($id) {

        if($id){
            $provider = Provider::where('id', $id)->with('profile')->first();

            if ($provider->profile) {
                $language = $provider->profile->language;
                App::setLocale($language);
            }

            $message = PushNotification::Message("Cadastro Ativado", [
                'badge' => 1,
                'sound' => 'default',
                'custom' => ['type' => 'approveProvider']
            ]);

            return $this->sendPushToProvider($id, $message);
        }

    }

    public function DisapproveProvider($id) {

        if($id){
            $provider = Provider::where('id', $id)->with('profile')->first();

            if ($provider->profile) {
                $language = $provider->profile->language;
                App::setLocale($language);
            }

            $message = PushNotification::Message("Cadastro dasativado", [
                'badge' => 1,
                'sound' => 'default',
                'custom' => ['type' => 'disapproveProvider']
            ]);

            return $this->sendPushToProvider($id, $message);
        }

    }

    public function ProviderWaiting($user_id, $status) {

        $user = User::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        if ($status == 1) {

            $message = PushNotification::Message(trans('api.push.provider_waiting_start'), [
                'badge' => 1,
                'sound' => 'default',
                'custom' => ['type' => 'driverStartWait']
            ]);

            return $this->sendPushToUser($user_id, $message);
        } else {
            return $this->sendPushToUser($user_id, trans('api.push.provider_waiting_end'));
        }
    }

    /**
     * Notificação de viagem cancelada pelo motorista
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function ProviderCancellRide($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        $message = \PushNotification::Message(trans('api.push.provider_cancelled'), array(
            'badge' => 1,
            'sound' => 'default',
            'custom' => array('type' => 'driverCanceltrip')
        ));

        return $this->sendPushToUser($request->user_id, $message);
    }

    /**
     * Notificação que não possui motoristas disponíveis na localidade
     *
     * @param $user_id
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function ProviderNotAvailable($user_id) {

        $user = User::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        $message = \PushNotification::Message(trans('api.push.provider_not_available'), array(
            'badge' => 1,
            'sound' => 'default',
            'custom' => array('type' => 'driverNotAvailable')
        ));

        return $this->sendPushToUser($user_id, $message);

    }

    /**
     * Mensagem de pré autorização
     *
     * @param $id
     * @param $message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function preAutorizeTrip($id, $message)
    {

        $message = \PushNotification::Message($message, [
            'badge' => 1,
            'sound' => 'default',
            'custom' => ['type' => 'preAutorize']
        ]);

        return $this->sendPushToUser($id, $message);

    }

    /**
     * Mensagem de erro caso tenha falha na autorização
     * 
     * @param $id
     * @param $message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function preAutorizeError($id, $message)
    {

        $message = \PushNotification::Message($message, [
            'badge' => 1,
            'sound' => 'default',
            'custom' => ['type' => 'preAutorizeError']
        ]);

        return $this->sendPushToUser($id, $message);

    }

    /**
     * Realiza captura da transação pré autorizada.
     * 
     * @param $id
     * @param $message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function captureAutorization($id, $message)
    {

        $message = \PushNotification::Message($message, [
            'badge' => 1,
            'sound' => 'default',
            'custom' => ['type' => 'captureAutorize']
        ]);

        return $this->sendPushToUser($id, $message);

    }

    /**
     * Notificação que o motorista chegou ao local de partida
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function Arrived($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($request->user_id, trans('api.push.arrived'));
    }

    /**
     * Notificação de viagem iniciada
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function Pickedup($request) {
        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($request->user_id, trans('api.push.pickedup'));
    }

    /**
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function Dropped($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($request->user_id, trans('api.push.dropped') . config('constants.currency') . $request->payment->total . ( $request->payment_mode == "CASH" ? " em DINHEIRO" : " via CARTÃO" ));
    }

    /**
     * Notificação de viagem finalizada
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function Complete($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($request->user_id, trans('api.push.complete'));
    }

    /**
     * Notificação de avaliação do motorista
     *
     * @param $request
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function Rate($request) {

        $user = User::where('id', $request->user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($request->user_id, trans('api.push.rate'));
    }

    /**
     * Notificação de chat para o motorista
     *
     * @param $provider
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function chatToProvider($provider)
    {
        $provider = Provider::where('id', $provider)->with('profile')->first();
        if ($provider->profile) {
            $language = $provider->profile->language;
            App::setLocale($language);
        }

        $message = \PushNotification::Message("Nova mensagem", array(
            'badge' => 1,
            'sound' => 'default',
            'custom' => array('type' => 'chat')
        ));

        return $this->sendPushToProvider($provider->id, $message);
    }

    /**
     * Notificação de chat para o Passageiro
     *
     * @param $provider
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function chatToUser($user_id)
    {
        $user = User::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        $message = \PushNotification::Message("Nova mensagem", array(
            'badge' => 1,
            'sound' => 'default',
            'custom' => array('type' => 'chat')
        ));

        return $this->sendPushToUser($user->id, $message);
    }

    /**
     * Notificação de nova viagem para o motorista
     *
     * @param $provider
     * @param $message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function IncomingRequest($provider) {

        $provider = Provider::where('id', $provider)->with('profile')->first();
        if ($provider->profile) {
            $language = $provider->profile->language;
            App::setLocale($language);
        }

        $message = \PushNotification::Message(trans('api.push.incoming_request'), array(
            'badge' => 1,
            'sound' => 'default',
            'custom' => array('type' => 'newRequest')
        ));

        return $this->sendPushToProvider($provider->id, $message);
    }

    /**
     * Notificação que os documentos foram verificados
     *
     * @param $provider_id
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function DocumentsVerfied($provider_id) {

        $provider = Provider::where('id', $provider_id)->with('profile')->first();
        if ($provider->profile) {
            $language = $provider->profile->language;
            App::setLocale($language);
        }

        return $this->sendPushToProvider($provider_id, trans('api.push.document_verfied'));
    }

    /**
     * Notificação de créditos adicionado em carteira ao passageiro
     *
     * @param $user_id
     * @param $money
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function WalletMoney($user_id, $money) {

        $user = User::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);
        return $this->sendPushToUser($user_id, $money . ' ' . trans('api.push.added_money_to_wallet'));
    }

    /**
     * Notificação de crédito em carteira ao motorista
     *
     * @param $user_id
     * @param $money
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function ProviderWalletMoney($user_id, $money) {

        $user = Provider::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToProvider($user_id, $money . ' ' . trans('api.push.added_money_to_wallet'));
    }

    /**
     * Notificação de recarga em carteira
     *
     * @param $user_id
     * @param $money
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function ChargedWalletMoney($user_id, $money) {

        $user = User::where('id', $user_id)->first();
        $language = $user->language;
        App::setLocale($language);

        return $this->sendPushToUser($user_id, $money . ' ' . trans('api.push.charged_from_wallet'));
    }

    /**
     * Coloca o motorista em espera
     *
     * @param $provider_id
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function provider_hold($provider_id) {

        $provider = Provider::where('id', $provider_id)->with('profile')->first();
        if ($provider->profile) {
            $language = $provider->profile->language;
            App::setLocale($language);
        }

        return $this->sendPushToProvider($provider_id, trans('api.push.provider_status_hold'));
    }

    /**
     * Envia o push para o usuário
     *
     * @param $user_id
     * @param $push_message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function sendPushToUser($user_id, $push_message) {

        try {

            $user = User::findOrFail($user_id);

            if ($user->device_token != "") {

                if ($user->device_type == 'ios') {

                    $message = $push_message;
                    $message = \PushNotification::Message($message, array(
                        'badge' => 1,
                        'sound' => 'default'
                    ));

                    return \PushNotification::app('IOSUser')
                        ->to($user->device_token)
                        ->send($message);
                } elseif ($user->device_type == 'android') {

                    return \PushNotification::app('Android')
                        ->to($user->device_token)
                        ->send($push_message);
                }
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    /**
     * Envia o push para o motorista
     *
     * @param $provider_id
     * @param $push_message
     * @return \Davibennun\LaravelPushNotification\App|Exception
     */
    public function sendPushToProvider($provider_id, $push_message) {

        try {

            $provider = ProviderDevice::where('provider_id', $provider_id)->with('provider')->first();

            if ($provider->token != "") {

                if ($provider->type == 'ios') {

                    $message = $push_message;
                    $message = \PushNotification::Message($message, array(
                        'badge' => 1,
                        'sound' => 'default'
                    ));

                    return \PushNotification::app('IOSProvider')
                        ->to($provider->token)
                        ->send($message);
                } elseif ($provider->type == 'android') {

                    return \PushNotification::app('Android')
                        ->to($provider->token)
                        ->send($push_message);
                }
            }
        } catch (Exception $e) {
            return $e;
        }
    }

}
