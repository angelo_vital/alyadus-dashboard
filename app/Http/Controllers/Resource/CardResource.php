<?php

namespace App\Http\Controllers\Resource;

use App\Jobs\AttachCardToUser;
use App\Services\Zoop\ZoopService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Card;
use Exception;
use Auth;
use Illuminate\Http\Response;

class CardResource extends Controller
{
    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try{

            $cards = Card::where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
            return $cards;

        } catch(Exception $e){
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'holder_name' => 'required',
            'expiration_month' => 'required',
            'expiration_year' => 'required',
            'card_number' => 'required',
            'security_code' => 'required',
        ]);

        try{

            $cardResponse = (new ZoopService())->addCard($request->all());

            $exist = Card::where('user_id',Auth::user()->id)
                ->where('last_four', $cardResponse['last_four'])
                ->where('brand', $cardResponse['brand'])
                ->count();

            if($exist == 0){

                $create_card = new Card;
                $create_card->user_id = Auth::user()->id;
                $create_card->card_id = $cardResponse['id'];
                $create_card->token_id = $cardResponse['token'];
                $create_card->last_four = $cardResponse['last_four'];
                $create_card->brand = $cardResponse['brand'];
                $create_card->save();

//                AttachCardToUser::dispatch($cardResponse['token'], Auth::user()->buyer_id);
                (new ZoopService())->attachCardToUser($cardResponse['token'], Auth::user()->buyer_id);

            }else{
                if($request->ajax()){
                    return response()->json(['message' => trans('api.card_already')], Response::HTTP_BAD_REQUEST);
                }else{
                    return back()->with('flash_error',trans('api.card_already'));
                }
            }

            if($request->ajax()){
                return response()->json(['message' => trans('api.card_added')]);
            }else{
                return back()->with('flash_success',trans('api.card_added'));
            }

        } catch(Exception $e){
            if($request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove Cartão do banco
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy(Request $request)
    {

        $this->validate($request,[
            'card_id' => 'required|exists:cards,card_id,user_id,'.Auth::user()->id,
        ]);

        try{

            Card::where('card_id',$request->card_id)->delete();

            if($request->ajax()){
                return response()->json(['message' => trans('api.card_deleted')]);
            }else{
                return back()->with('flash_success',trans('api.card_deleted'));
            }

        } catch(Exception $e){
            if($request->ajax()){
                return response()->json(['error' => $e->getMessage()], 500);
            }else{
                return back()->with('flash_error',$e->getMessage());
            }
        }
    }

    /**
     * setting stripe.
     *
     * @return \Illuminate\Http\Response
     */
    public function set_stripe(){
        return \Stripe\Stripe::setApiKey(config('constants.stripe_secret_key', ''));
    }

    /**
     * Get a stripe customer id.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_id()
    {
        if(Auth::user()->stripe_cust_id != null){

            return Auth::user()->stripe_cust_id;

        }else{

            try{

                $stripe = $this->set_stripe();

                $customer = \Stripe\Customer::create([
                    'email' => Auth::user()->email,
                ]);

                User::where('id',Auth::user()->id)->update(['stripe_cust_id' => $customer['id']]);
                return $customer['id'];

            } catch(Exception $e){
                return $e;
            }
        }
    }

}
