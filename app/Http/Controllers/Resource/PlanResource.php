<?php

namespace App\Http\Controllers\Resource;

use App\Plan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanResource extends Controller
{
    
    /**
     * Exibe lista de planos
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $plans = Plan::all();

        return view('admin.plan.index', compact('plans'));
    }

    /**
     * Exibe formulario de cadastro de planos
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.plan.create');
    }

    /**
     * Cadastro novo plano
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $plan = Plan::create($request->all());

        if($plan){
            return redirect()->route('admin.plan.index');
        }
        return redirect()->route('admin.plan.index')->withErrors("Erro ao cadastrar plano");
    }

    /**
     * Exibe plano atual
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $plan = Plan::find($id);

        return view('admin.plan.create', compact('plan'));
    }

    /**
     * Exibe plano para edição
     *
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $plan = Plan::find($id);

        return view('admin.plan.edit', compact('plan'));
    }

    /**
     * Atualiza plano
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $plan = Plan::find($id)->update($request->all());

        if($plan){
            return redirect()->route('admin.plan.index');
        }
        return redirect()->route('admin.plan.index')->withErrors("Erro ao atualizar plano");
    }

    /**
     * Remove plano do banco de dados
     *
     * @param $id
     * @return string
     * @throws \Exception
     */
    public function destroy($id)
    {
        Plan::find($id)->delete();

        return redirect(url()->previous());
    }

}
