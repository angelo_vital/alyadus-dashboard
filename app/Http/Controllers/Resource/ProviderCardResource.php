<?php

namespace App\Http\Controllers\Resource;

use App\Exceptions\ZoopCardAddException;
use App\Jobs\AttachCardToUser;
use App\Services\Zoop\ZoopService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProviderCard;
use App\Provider;
use Exception;
use Auth;
use Setting;
use Session;

class ProviderCardResource extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        try {

            $cards = ProviderCard::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
            return $cards;
        } catch (Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //  
    }

    /**
     * Realiza cadastro do cartão
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request) {

        $this->validate($request,[
            'holder_name' => 'required',
            'expiration_month' => 'required',
            'expiration_year' => 'required',
            'card_number' => 'required',
            'security_code' => 'required',
        ]);

        try {


            $cardResponse = (new ZoopService())->addCard($request->all());

            $exist = ProviderCard::where('user_id', Auth::user()->id)
                    ->where('last_four', $cardResponse['last_four'])
                    ->where('brand', $cardResponse['brand'])
                    ->count();

            if ($exist == 0) {

                $Providercard = ProviderCard::where('user_id', Auth::user()->id)->first();

                if (!empty($Providercard)) {
                    ProviderCard::where('card_id', $Providercard->card_id)->delete();
                }

                $create_card = new ProviderCard;
                $create_card->user_id = Auth::user()->id;
                $create_card->token_id = $cardResponse['token'];
                $create_card->card_id = $cardResponse['id'];
                $create_card->last_four = $cardResponse['last_four'];
                $create_card->brand = $cardResponse['brand'];
                $create_card->is_default = '1';
                $create_card->save();

                AttachCardToUser::dispatch($cardResponse['token'], Auth::user()->seller_id);

                //ATUALIZA STATUS DO USUÁRIO AO ADICIONAR CARTÃO
//                Provider::where('id', Auth::user()->id)->where('status', 'card')->update(['status' => 'onboarding']);
            } else {
                if ($request->ajax()) {
                    return response()->json(['message' => trans('api.card_already')]);
                } else {
                    return back()->with('flash_success', trans('api.card_already'));
                }
            }

            if ($request->ajax()) {
                return response()->json(['message' => trans('api.card_added')]);
            } else {
                return back()->with('flash_success', trans('api.card_added'));
            }
        } catch (ZoopCardAddException $e){
            if ($request->ajax()) {
                return response()->json(['error' => $e->getMessage()], 520);
            } else {
                return back()->with('flash_error', $e->getMessage());
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'card_id' => 'required|exists:provider_cards,card_id,user_id,' . Auth::user()->id,
        ]);
        try {
            ProviderCard::where('user_id', Auth::user()->id)->update(['is_default' => '0']);
            ProviderCard::where('card_id', $request->card_id)->update(['is_default' => '1']);
            return 'success';
        } catch (Exception $e) {
            return 'failure';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {

        $this->validate($request, [
            'card_id' => 'required|exists:provider_cards,card_id,user_id,' . Auth::user()->id,
        ]);

        try {

            $this->set_stripe();

            $customer = \Stripe\Customer::retrieve(Auth::user()->stripe_cust_id);
            $customer->sources->retrieve($request->card_id)->delete();

            ProviderCard::where('card_id', $request->card_id)->delete();

            if ($request->ajax()) {
                return response()->json(['message' => trans('api.card_deleted')]);
            } else {
                return back()->with('flash_success', trans('api.card_deleted'));
            }
        } catch (Stripe_CardError $e) {
            if ($request->ajax()) {
                return response()->json(['error' => $e->getMessage()], 500);
            } else {
                return back()->with('flash_error', $e->getMessage());
            }
        }
    }


    /**
     * Get a stripe customer id.
     *
     * @return \Illuminate\Http\Response
     */
    public function customer_id() {
        if (Auth::user()->stripe_cust_id != null) {

            return Auth::user()->stripe_cust_id;
        } else {

            try {

                $stripe = $this->set_stripe();

                $customer = \Stripe\Customer::create([
                            'email' => Auth::user()->email,
                ]);

                Provider::where('id', Auth::user()->id)->update(['stripe_cust_id' => $customer['id']]);
                return $customer['id'];
            } catch (Exception $e) {
                return $e;
            }
        }
    }

    public function set_default(Request $request) {
        try {
            ProviderCard::where('user_id', Auth::user()->id)->update(['is_default' => '0']);
            ProviderCard::where('id', $request->id)->update(['is_default' => '1']);
            return 'success';
        } catch (Exception $e) {
            return 'failure';
        }
    }

}