<?php

namespace App\Http\Controllers\Resource;

use App\Document;
use App\Http\Controllers\ProviderResources\DocumentController;
use App\Services\Zoop\ZoopService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SendPushNotification;

use DB;
use Exception;
use Setting;

use App\Provider;
use App\ServiceType;
use App\ProviderService;
use App\ProviderDocument;
use App\Helpers\Helper;

class ProviderDocumentResource extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:provider-service-update', ['only' => ['store']]);
        $this->middleware('permission:provider-service-delete', ['only' => ['service_destroy']]);
        $this->middleware('permission:provider-document-edit', ['only' => ['update']]);
        $this->middleware('permission:provider-document-delete', ['only' => ['destroy']]);
    }

    /**
     * @param Request $request
     * @param $provider
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request, $provider)
    {
        try {
            $backurl=$request->session()->get('providerpage');
            $Provider = Provider::findOrFail($provider);
            $ProviderService = ProviderService::where('provider_id',$provider)->with('service_type')->get();
            $ServiceTypes = ServiceType::all();
            $documentTypes = Document::all(['id'])->count();
            return view('admin.providers.document.index', compact('Provider', 'ServiceTypes','ProviderService','backurl','documentTypes'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($provider)
    {
        $provider = Provider::where('id', $provider)->firstOrFail();
        $documents = Document::orderBy('created_at' , 'desc')->get();

        return view('admin.providers.document.create',compact('documents','provider'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $provider)
    {
        if ($request->type == 'documents')
        {
            $this->validate($request, [
                'document*' => 'mimes:jpg,jpeg,png',
            ]);

            try{
                foreach ($request->file() as $docKey => $image){
                    $idDoc = str_replace('document','',$docKey);

                    (new DocumentController())->documentupdate($image, $idDoc,$provider);
                }
                return redirect()->route('admin.provider.document.index', $provider)->with('flash_success', 'Documentos Salvos');
            } catch (Exception $e ){
                return redirect()
                    ->route('admin.provider.document.index', $provider)
                    ->with('flash_error','Ocorreu um erro ao salvar os documentos enviados');
            }
        } else {
            $this->validate($request, [
                'service_type' => 'required|exists:service_types,id',
                'service_number' => 'required',
                'service_model' => 'required',
            ]);


            try {

                $ProviderService = ProviderService::where('provider_id', $provider)->firstOrFail();
                $ProviderService->update([
                    'service_type_id' => $request->service_type,
                    'status' => 'offline',
                    'service_number' => $request->service_number,
                    'service_model' => $request->service_model,
                ]);

                // Sending push to the provider
                (new SendPushNotification)->DocumentsVerfied($provider);

            } catch (ModelNotFoundException $e) {
                ProviderService::create([
                    'provider_id' => $provider,
                    'service_type_id' => $request->service_type,
                    'status' => 'offline',
                    'service_number' => $request->service_number,
                    'service_model' => $request->service_model,
                ]);
            }

            return redirect()->route('admin.provider.document.index', $provider)->with('flash_success', trans('admin.provider_msgs.provider_service_update'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($provider, $id)
    {
        try {
            $Document = ProviderDocument::where('provider_id', $provider)
                ->findOrFail($id);

            return view('admin.providers.document.edit', compact('Document'));
        } catch (ModelNotFoundException $e) {
            return redirect()->route('admin.index');
        }
    }

    /**
     * @param Request $request
     * @param $provider
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $provider, $id)
    {
        try {
            $providerModel = Provider::find($provider);
            $Document = ProviderDocument::where('provider_id', $provider)
                ->findOrFail($id);

            if($Document->document_id == 1){
                (new ZoopService())->sendSellerDocuments($providerModel->seller_id, $Document->url, 'identificacao');
            } else if($Document->document_id == 3){
                (new ZoopService())->sendSellerDocuments($providerModel->seller_id, $Document->url, 'residencia');
            }

            $Document->update(['status' => 'ACTIVE']);


            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', trans('admin.provider_msgs.document_approved'));
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', trans('admin.provider_msgs.document_not_found'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($provider, $id)
    {
        try {

            $Document = ProviderDocument::findOrFail($id);
            $Document->delete();

            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', trans('admin.provider_msgs.document_delete'));
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', trans('admin.provider_msgs.document_not_found'));
        }
    }

    /**
     * Delete the service type of the provider.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function service_destroy(Request $request, $provider, $id)
    {
        try {

            $ProviderService = ProviderService::where('provider_id', $provider)->firstOrFail();
            $ProviderService->delete();

            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_success', trans('admin.provider_msgs.provider_service_delete'));
        } catch (ModelNotFoundException $e) {
            return redirect()
                ->route('admin.provider.document.index', $provider)
                ->with('flash_error', trans('admin.provider_msgs.provider_service_not_found'));
        }
    }

}
