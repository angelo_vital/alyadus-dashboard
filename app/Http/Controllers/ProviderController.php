<?php

namespace App\Http\Controllers;

use App\Card;
use App\ProviderService;
use App\Reason;
use App\Services\PaymentGateway;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\UserRequests;
use App\UserRequestPayment;
use App\RequestFilter;
use App\ProviderWallet;
use App\Provider;
use App\WalletRequests;
use App\Notifications;
use App\Dispute;
use App\UserRequestDispute;
use App\PushSubscription;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\DB;
use Location\Coordinate;
use Location\Distance\Vincenty;
use Setting;
use App\Helpers\Helper;
use App\Http\Controllers\ProviderResources\TripController;
use App\Http\Controllers\Resource\ReferralResource;
use App\Http\Controllers\UserApiController;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('provider', ['except' => 'save_subscription']);
        $this->middleware('demo', ['only' => [
                'update_password',
            ]]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('provider.index');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function incoming(Request $request)
    {
        return (new TripController())->index($request);
    }

    /**
     * @param Request $request
     * @param $id
     * @return UserRequests|UserRequests[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Http\JsonResponse
     */
    public function accept(Request $request, $id)
    {
        return (new TripController())->accept($request, $id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function reject($id)
    {
        return (new TripController())->destroy($id);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \ErrorException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        return (new TripController())->update($request, $id);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function rating(Request $request, $id)
    {
        return (new TripController())->rate($request, $id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function earnings()
    {
        $provider = Provider::where('id',\Auth::guard('provider')->user()->id)
                    ->with('service','accepted','cancelled')
                    ->get();

        $weekly = UserRequests::where('provider_id',\Auth::guard('provider')->user()->id)
                    ->with('payment')
                    ->where('created_at', '>=', Carbon::now()->subWeekdays(7))
                    ->get();

        $weekly_sum = UserRequestPayment::whereHas('request', function($query) {
                        $query->where('provider_id',\Auth::guard('provider')->user()->id);
                        $query->where('created_at', '>=', Carbon::now()->subWeekdays(7));
                    })
                        ->sum('provider_pay');

        $today = UserRequests::where('provider_id',\Auth::guard('provider')->user()->id)
                    ->where('created_at', '>=', Carbon::today())
                    ->count();

        $fully = UserRequests::where('provider_id',\Auth::guard('provider')->user()->id)
                    ->with('payment','service_type')->orderBy('id','desc')
                    ->get();

        $fully_sum = UserRequestPayment::whereHas('request', function($query) {
                        $query->where('provider_id', \Auth::guard('provider')->user()->id);
                        })
                        ->sum('provider_pay');

        return view('provider.payment.earnings',compact('provider','weekly','fully','today','weekly_sum','fully_sum'));
    }

    /**
     * available.
     *
     * @return \Illuminate\Http\Response
     */
    public function available(Request $request)
    {
        (new ProviderResources\ProfileController)->available($request);
        return back();
    }

    /**
     * Show the application change password.
     *
     * @return \Illuminate\Http\Response
     */
    public function change_password()
    {
        return view('provider.profile.change_password');
    }

    /**
     * Change Password.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)
    {
        $this->validate($request, [
                'password' => 'required|confirmed',
                'old_password' => 'required',
            ]);

        $Provider = \Auth::user();

        if(password_verify($request->old_password, $Provider->password))
        {
            $Provider->password = bcrypt($request->password);
            $Provider->save();

            return back()->with('flash_success', trans('admin.password_update'));
        } else {
            return back()->with('flash_error', trans('admin.password_error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function location_edit()
    {
        return view('provider.location.index');
    }

    /**
     * Update latitude and longitude of the user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function location_update(Request $request)
    {
        $this->validate($request, [
                'latitude' => 'required|numeric',
                'longitude' => 'required|numeric',
            ]);

        if($Provider = \Auth::user()){

            $Provider->latitude = $request->latitude;
            $Provider->longitude = $request->longitude;
            $Provider->save();

            return back()->with(['flash_success' => trans('api.provider.location_updated')]);

        } else {
            return back()->with(['flash_error' => trans('admin.provider_msgs.provider_not_found')]);
        }
    }

    /**
     * upcoming history.
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming_trips()
    {
        $fully = (new ProviderResources\TripController)->upcoming_trips();
        return view('provider.payment.upcoming',compact('fully'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function cancel(Request $request) {
        try{

            (new TripController)->cancel($request);
            return back()->with(['flash_success' => trans('admin.provider_msgs.trip_cancelled')]);
        } catch (ModelNotFoundException $e) {
            return back()->with(['flash_error' => trans('admin.something_wrong')]);
        }
    }

    public function wallet_transation(Request $request){

        try{
            /*$wallet_transation = ProviderWallet::where('provider_id',Auth::user()->id)
                                ->orderBy('id','desc')
                                ->paginate(config('constants.per_page', '10'));*/


            $cards = (new Resource\ProviderCardResource)->index();

            $wallet_transation = ProviderWallet::with('transactions')->select('transaction_alias',\DB::raw('SUM(amount) as amount'))->where('provider_id',Auth::user()->id)->groupBy('transaction_alias')->paginate(config('constants.per_page', '10'));


            $pagination=(new Helper)->formatPagination($wallet_transation);

            $wallet_balance=Auth::user()->wallet_balance;

            if(config('constants.braintree') == 1) {
                (new UserApiController())->set_Braintree();
                $clientToken = \Braintree_ClientToken::generate();
            } else {
                $clientToken = '';
            }

            return view('provider.wallet.wallet_transation',compact('wallet_transation','pagination','wallet_balance','cards','clientToken'));

        }catch(Exception $e){
            return back()->with(['flash_error' => trans('admin.something_wrong')]);
        }

    }

    public function wallet_details(Request $request){

        try{

            $wallet_details = ProviderWallet::where('transaction_alias','LIKE', $request->alias_id)->where('provider_id',Auth::user()->id)->get();

            return response()->json(['data' => $wallet_details]);

        }catch(Exception $e){
            return response()->json(['error' => trans('api.something_went_wrong')], 500);
        }

    }

    public function transfer(Request $request){

        $pendinglist = WalletRequests::where('from_id',Auth::user()->id)->where('request_from','provider')->where('status',0)->get();
        $wallet_balance=Auth::user()->wallet_balance;
        return view('provider.wallet.transfer',compact('pendinglist','wallet_balance'));
    }

    public function requestamount(Request $request)
    {


        $send=(new TripController())->requestamount($request);
        $response=json_decode($send->getContent(),true);

        if(!empty($response['error']))
            $result['flash_error']=$response['error'];
        if(!empty($response['success']))
            $result['flash_success']=$response['success'];

        return redirect()->back()->with($result);
    }

    public function requestcancel(Request $request)
    {

        $cancel=(new TripController())->requestcancel($request);
        $response=json_decode($cancel->getContent(),true);

        if(!empty($response['error']))
            $result['flash_error']=$response['error'];
        if(!empty($response['success']))
            $result['flash_success']=$response['success'];

        return redirect()->back()->with($result);
    }


    public function stripe(Request $request)
    {
        return (new ProviderResources\ProfileController)->stripe($request);
    }

    public function cards()
    {
        $cards = (new Resource\ProviderCardResource)->index();
        return view('provider.wallet.card',compact('cards'));
    }

    public function referral()
    {
        if(config('constants.referral') == 0){
            return redirect('provider');
        }
        $referrals  = (new ReferralResource)->get_referral(2,Auth::user()->id);
        return view('provider.referral',compact('referrals'));
    }
    public function notifications()
    {
        $notifications = Notifications::where([['notify_type','!=','user'],['status','active']])
                                       ->orderBy('created_at' , 'desc')
                                       ->get();
        return view('provider.notification.index',compact('notifications'));
    }

         /**
     * Dispute.
     *
     * @return \Illuminate\Http\Response
     */
    public function dispute($id)
    {

         $dispute = UserRequestDispute::where([['request_id',$id],['dispute_type','!=','user']])
                                        ->get();
         $closedStatus = UserRequestDispute::where([['request_id',$id],['status','closed'],['dispute_type','!=','user']])
                                        ->first();
         $disputeReason = Dispute::where([['dispute_type','provider'],['status','active']])
                                        ->get();
              $sendBtn = ($closedStatus)?"yes":"no";
    return response()->json(['dispute' => $dispute,'sendBtn' => $sendBtn,'disputeReason'=>$disputeReason]);
    }
      /**
     * Dispute Save.
     *
     * @return \Illuminate\Http\Response
     */
    public function dispute_store(Request $request)
    {
            try{
                $dispute = new UserRequestDispute;
                $dispute->request_id = $request->request_id;
                $dispute->user_id = Auth::user()->id;
                $dispute->dispute_title = $request->dispute_title;
                $dispute->dispute_name = $request->dispute_name;
                $dispute->dispute_type = 'provider';
                if($request->has('comments')) {
                    $dispute->comments = $request->comments;
                }

                $dispute->save();

                    return response()->json(['message' => 'success']);

            }

            catch (ModelNotFoundException $e) {
                return back()->with('flash_error', 'error');
            }

    }



    public function save_subscription($id, Request $request) {

        $user = Provider::findOrFail($id);

        $endpoint = $request->input('endpoint');
        $key = $request->input('keys.p256dh');
        $token = $request->input('keys.auth');
        $guard = 'provider';

        $subscription = PushSubscription::findByEndpoint($endpoint);

        if ($subscription && $subscription->user_id == $id) {
            $subscription->guard = $guard;
            $subscription->public_key = $key;
            $subscription->auth_token = $token;
            $subscription->save();

            return $subscription;
        }

        if ($subscription && ! $subscription->user_id == $id) {
            $subscription->delete();
        }

        $subscribe = new PushSubscription();
        $subscribe->user_id = $user->id;
        $subscribe->guard = $guard;
        $subscribe->endpoint = $endpoint;
        $subscribe->public_key = $key;
        $subscribe->auth_token = $token;
        $subscribe->save();

        return response()->json([ 'success' => true ]);
    }

    public function calculate_distance($request, $id)
    {
        $this->validate($request, [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);
        try {

            if ($request->ajax()) {
                $Provider = Auth::user();
            } else {
                $Provider = Auth::guard('provider')->user();
            }

            $UserRequest = UserRequests::where('status', 'PICKEDUP')
                ->where('provider_id', $Provider->id)
                ->find($id);

            if ($UserRequest && ($request->latitude && $request->longitude)) {

                //Log::info("REQUEST ID:" . $UserRequest->id . "==SOURCE LATITUDE:" . $UserRequest->track_latitude . "==SOURCE LONGITUDE:" . $UserRequest->track_longitude);

                if ($UserRequest->track_latitude && $UserRequest->track_longitude) {

                    $coordinate1 = new Coordinate($UserRequest->track_latitude, $UserRequest->track_longitude);
                    /** Set Distance Calculation Source Coordinates *** */
                    $coordinate2 = new Coordinate($request->latitude, $request->longitude);
                    /** Set Distance calculation Destination Coordinates *** */
                    $calculator = new Vincenty();

                    /*                     * *Distance between two coordinates using spherical algorithm (library as mjaschen/phpgeo) ** */

                    $mydistance = $calculator->getDistance($coordinate1, $coordinate2);

                    $meters = round($mydistance);

                    //Log::info("REQUEST ID:" . $UserRequest->id . "==BETWEEN TWO COORDINATES DISTANCE:" . $meters . " (m)");

                    if ($meters >= 100) {
                        /*                         * * If traveled distance riched houndred meters means to be the source coordinates ** */
                        $traveldistance = round(($meters / 1000), 8);

                        $calulatedistance = $UserRequest->track_distance + $traveldistance;

                        $UserRequest->track_distance = $calulatedistance;
//                        $UserRequest->distance = $calulatedistance;
//                        $UserRequest->track_latitude = $request->latitude;
//                        $UserRequest->track_longitude = $request->longitude;
                        $UserRequest->save();
                    }
                } else if (!$UserRequest->track_latitude && !$UserRequest->track_longitude) {
//					$UserRequest->distance             = 0;
//                    $UserRequest->track_latitude = $request->latitude;
//                    $UserRequest->track_longitude = $request->longitude;
                    $UserRequest->save();
                }
            }
            return $UserRequest;
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => trans('api.something_went_wrong')]);
        }
    }

    public function assign_destroy($id)
    {
        $UserRequest = UserRequests::find($id);
        try {
            UserRequests::where('id', $UserRequest->id)->update(['status' => 'CANCELLED']);
            // No longer need request specific rows from RequestMeta
            RequestFilter::where('request_id', $UserRequest->id)->delete();
            //  request push to user provider not available
            (new SendPushNotification)->ProviderNotAvailable($UserRequest->user_id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => trans('api.unable_accept')]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('api.connection_err')]);
        }
    }

    public function assign_next_provider($request_id)
    {

        try {
            $UserRequest = UserRequests::findOrFail($request_id);
        } catch (ModelNotFoundException $e) {
            // Cancelled between update.
            return false;
        }

        RequestFilter::where('provider_id', $UserRequest->current_provider_id)
            ->where('request_id', $UserRequest->id)
            ->delete();

        try {

            $next_provider = RequestFilter::where('request_id', $UserRequest->id)
                ->orderBy('id')
                ->firstOrFail();

            $UserRequest->current_provider_id = $next_provider->provider_id;
            $UserRequest->assigned_at = Carbon::now();
            $UserRequest->save();

            (new SendPushNotification)->IncomingRequest($next_provider->provider_id);
        } catch (ModelNotFoundException $e) {

            UserRequests::where('id', $UserRequest->id)->update(['status' => 'CANCELLED']);

            RequestFilter::where('request_id', $UserRequest->id)->delete();

            (new SendPushNotification)->ProviderNotAvailable($UserRequest->user_id);
        }
    }

}
