<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\NumberValidation;
use App\Provider;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SmsController extends Controller
{
    public function send(Request $request)
    {

        if($this->invalidatePhone($request->get('mobile'))){
            return response()->json(['message' => "Telefone informado é inválido"], Response::HTTP_BAD_REQUEST);
        }

        if($request->has("app")){
            if ($this->phoneExists($request->get("code"),$request->get("mobile"), $request->get("app"))) {
                return response()->json(['message' => trans('api.mobile_exist')], Response::HTTP_BAD_REQUEST);
            }
        }else{
            if ($this->phoneExists($request->get("code"),$request->get("mobile"))) {
                return response()->json(['message' => trans('api.mobile_exist')], Response::HTTP_BAD_REQUEST);
            }
        }

        $otp = mt_rand(1000, 9999);
        $phone = $request->get('code').$request->get('mobile');

        $result = NumberValidation::create([
            'code' => $request->get('code'),
            'mobile' => $request->get('mobile'),
            'otp' => $otp
        ]);

        if ($result) {
            $result = Helper::send_sms("Seu código de verificação é: {$otp}", $phone);

            if ($result) {
                return response()->json([
                    'message' => 'Código enviado para o seu telefone!',
                ], Response::HTTP_OK);
            } else {
                return response()->json([
                    'message' => 'Não foi possível enviar o código.',
                ], Response::HTTP_BAD_REQUEST);
            }
        } else {
            return response()->json([
                'message' => 'Não foi possível enviar o código.',
            ], Response::HTTP_BAD_REQUEST);
        }

    }

    public function validateSms(Request $request)
    {
        try{

            $validCode = NumberValidation::where('mobile', $request->get('mobile'))
                ->where('otp', $request->get('otp'))
                ->get();

            if(count($validCode) >= 1){

                //$validCode->delete();

                return response()->json([
                    'message' => 'Código válidado com sucesso!!',
                ], Response::HTTP_OK);
            }

            return response()->json([
                'message' => 'Código inválido, tente novamente!!',
            ], Response::HTTP_BAD_REQUEST);

        }catch (\Exception $e){
            return response()->json([
               'message' => $e->getMessage()
            ]);
        }
    }

    private function invalidatePhone($mobile)
    {
        $invalidPhones = array(
            '11111111111','22222222222','33333333333','44444444444','55555555555','66666666666','77777777777','88888888888','99999999999',
            '10101010101', '20202020202','30303030303', '40404040404', '50505050505','60606060606','70707070707','80808080808','90909090909',
            '12345678912'
        );

        if(in_array($mobile, $invalidPhones) || strlen($mobile) > 11 || strlen($mobile) < 11){
            return true;
        }else{
            return false;
        }
    }

    private function phoneExists($code, $mobile, $app = ""){
        if($app == "driver"){
            $user = Provider::where('country_code', $code)
                ->where('mobile', $mobile)
                ->first();
        }elseif ($app == "user"){
            $user = User::where('country_code', $code)
                ->where('mobile', $mobile)
                ->first();
        } else{
            $user = User::where('country_code', $code)
                ->where('mobile', $mobile)
                ->first();
        }

        if ($user) {
            return true;
        }

        return false;
    }
}
