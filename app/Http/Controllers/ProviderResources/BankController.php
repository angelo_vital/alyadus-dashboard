<?php

namespace App\Http\Controllers\ProviderResources;

use App\Bank;
use App\Provider;
use App\ProviderBank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('provider.api');
    }

    public function index()
    {
        try{
            $providerBanks = Provider::findOrFail(Auth::user()->id)->banks()->with('bank')->get();

            return response()->json($providerBanks);
        }catch (\Exception $e){
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function list()
    {
        $banks = Bank::all();

        return response()->json($banks);
    }

    public function store(Request $request)
    {
        try{
            $data = $request->all();
            $data['type'] = ($request->type == 'Corrente'? 'CC' : 'CP');

            Provider::findOrFail(Auth::user()->id)->banks()->create($data);

            return response()->json(['success' => true], Response::HTTP_OK);
        }catch (\Exception $e){
            return response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    public function check()
    {
        try{

            $response = Provider::findOrFail(Auth::user()->id)->banks()->get();

            if(count($response) >= 1){
                return response()->json(['status' => 'accountok']);
            }
            return response()->json(['status' => 'noaccount']);

        }catch (\Exception $e){
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try{
            ProviderBank::findOrFail($id)->delete();

            return response()->json(['success' => true]);
        }catch (\Exception $e){
            return response()->json(['error' => $e->getMessage()]);
        }
    }

}
