<?php

namespace App\Http\Controllers\ProviderResources;

use App\Provider;
use App\ReferralHistroy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReferralController extends Controller
{

    public function __construct()
    {
        $this->middleware('provider.api');
    }

    public function showAllIndicated()
    {
        try {

            $firstLevel = $this->checkFirstLevel();
            $secondLevel = $this->checkSecondLevel();
            $thirdLevel = $this->checkThirdLevel();

            return response()->json(
                [
                    'total' => $firstLevel->count() + $secondLevel->count() + $thirdLevel->count(),
                    'first' => $firstLevel,
                    'second' => $secondLevel,
                    'third' => $thirdLevel
                ]
            );

        } catch (ModelNotFoundException $e) {
            Log::error("Class: ReferralController, " . "Error: " . $e->getMessage());
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    private function checkFirstLevel()
    {

        $providerModel = Provider::findOrFail(Auth::user()->id);

        return $providerModel->referrer()->with(['indicated' => function ($query) {
            $query->select(['id', 'first_name', 'last_name']);
        }])->whereHas("indicated", function ($q) use ($providerModel) {
            $q->where('referrer_id', $providerModel->id);
        })
            ->where('status', 'P')
            ->get(['type', 'referral_id', 'status']);

    }

    private function checkSecondLevel()
    {

        $providerModel = Provider::find(Auth::user()->id);

        $providerIdentifiers = $providerModel->referrer()->whereHas("indicated", function ($q) use ($providerModel) {
            $q->where('referrer_id', $providerModel->id);
        })->get()->pluck('referral_id');

        return ReferralHistroy::with(['indicated' => function ($query) use ($providerIdentifiers) {
            $query->select(['id', 'first_name', 'last_name']);
        }])
            ->whereIn('referrer_id', $providerIdentifiers)
            ->where('status', 'P')
            ->get(['type', 'referral_id', 'status']);
    }

    private function checkThirdLevel()
    {

        $providerModel = Provider::find(Auth::user()->id);

        $providerIdentifiers = $providerModel->referrer()->whereHas("indicated", function ($q) use ($providerModel) {
            $q->where('referrer_id', $providerModel->id);
        })->get()->pluck('referral_id');

        $providersSecond = ReferralHistroy::whereIn('referrer_id', $providerIdentifiers)->get()->pluck('referral_id');

        return ReferralHistroy::with(['indicated' => function ($query) use ($providerIdentifiers) {
            $query->select(['id', 'first_name', 'last_name']);
        }])
            ->where('status', 'P')
            ->whereIn('referrer_id', $providersSecond)
            ->get(['type', 'referral_id', 'status']);

    }

}
