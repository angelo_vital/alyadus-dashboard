<?php

namespace App\Http\Controllers\ProviderResources;

use App\City;
use App\Http\Controllers\SendPushNotification;
use App\Provider;
use App\ProviderService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProviderServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('provider.api');
    }

    public function update(Request $request)
    {

        $this->validate($request, [
            'service_type' => 'required|exists:service_types,id',
        ]);

        try {

            $citySelected = City::where('title', $request->city)->first();

            Provider::find(Auth::user()->id)->update(['city_id' => $citySelected->id]);

            $ProviderService = ProviderService::where('provider_id', Auth::user()->id)->firstOrFail();
            $ProviderService->update([
                'service_type_id' => $request->service_type,
                'status' => 'active',
            ]);

            // Sending push to the provider
            (new SendPushNotification)->DocumentsVerfied(Auth::user()->id);
            return response()->json([
               'success' => true,
               'message' => 'Cidade alterada com sucesso!'
            ]);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => "Não foi possivel alterar registro"
            ], Response::HTTP_BAD_REQUEST);
        }

    }
}
