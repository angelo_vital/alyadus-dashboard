<?php

namespace App\Http\Controllers\ProviderResources;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Document;
use App\ProviderDocument;
use App\Provider;
use Setting;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $VehicleDocuments = Document::vehicle()->get();
        $DriverDocuments = Document::driver()->get();

        $Provider = \Auth::guard('provider')->user();

        return view('provider.document.index', compact('DriverDocuments', 'VehicleDocuments', 'Provider'));
    }

    /**
     * Atualiza documentos
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
                'document' => 'mimes:jpg,jpeg,png',
            ]);

        //Log::info($request->all());

        try {

            $Document = ProviderDocument::where('provider_id', \Auth::guard('provider')->user()->id)
                ->where('document_id', $id)->with('provider')->with('document')
                ->firstOrFail();

            Storage::delete($Document->url);

            $filename=str_replace(" ","",$Document->document->name);

            $ext = $request->file('document')->guessExtension();

            $path = $request->file('document')->storeAs(
                "provider/documents/".$Document->provider_id, $filename.'.'.$ext
            );

            $Document->update([
                    'url' => $path,
                    'status' => 'ASSESSING',
                ]);



        } catch (ModelNotFoundException $e) {

            $document = Document::find($id);
            $filename=str_replace(" ","",$document->name);
            $ext = $request->file('document')->guessExtension();
            $path = $request->file('document')->storeAs(
                "provider/documents/".\Auth::guard('provider')->user()->id, $filename.'.'.$ext
            );
            ProviderDocument::create([
                    'url' => $path,
                    'provider_id' => \Auth::guard('provider')->user()->id,
                    'document_id' => $id,
                    'status' => 'ASSESSING',
                ]);

        }



        //update document to card status
        $total = Document::count();
        $provider_total = ProviderDocument::where('provider_id', \Auth::guard('provider')->user()->id)->count();

        if($total==$provider_total){
            if(config('constants.card', 0) == 1) {
                Provider::where('id',\Auth::guard('provider')->user()->id)->where('status','document')->update(['status'=>'onboarding']);
            }
            else{
                if(Setting::get('demo_mode', 0) == 1) {
                    Provider::where('id',\Auth::guard('provider')->user()->id)->where('status','document')->update(['status'=>'approved']);
                }
                else{
                    Provider::where('id',\Auth::guard('provider')->user()->id)->where('status','document')->update(['status'=>'onboarding']);
                }
            }
        }

        return back();
    }

    public function documentupdate($image, $id,$provider_id)
    {

        try {

            $Document = ProviderDocument::where('provider_id', $provider_id)
                ->where('document_id', $id)->with('provider')->with('document')
                ->firstOrFail();

            Storage::delete($Document->url);

            $filename=str_replace(" ","",$Document->document->name);

            $ext = $image->guessExtension();

            $path = $image->storeAs(
                "provider/documents/".$Document->provider_id, $filename.'.'.$ext
            );

            $Document->update([
                    'url' => $path,
                    'status' => 'ASSESSING',
                ]);

        } catch (ModelNotFoundException $e) {

            $document = Document::find($id);
            $filename=str_replace(" ","",$document->name);
            $ext = $image->guessExtension();
            $path = $image->storeAs(
                "provider/documents/".$provider_id, $filename.'.'.$ext
            );
            ProviderDocument::create([
                    'url' => $path,
                    'provider_id' => $provider_id,
                    'document_id' => $id,
                    'status' => 'ASSESSING',
                ]);

        }

        return true;
    }
}
