<?php

namespace App\Http\Controllers\ProviderResources;

use App\Exceptions\BoletoErrorException;
use App\Exceptions\ZoopFailedPayment;
use App\Jobs\ProviderPaidStatus;
use App\Notifications\SendSubscriptionMail;
use App\Plan;
use App\Provider;
use App\Services\Zoop\ZoopService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PlanController extends Controller
{
    public function index()
    {
        $plans = Plan::all();

        return response()->json($plans, Response::HTTP_OK);
    }

    public function boletoPay(Request $request)
    {

        try {

            $data = [];

            $plan = Plan::find($request->get('plan_id'));
            $subscription = Provider::find(Auth::user()->id)->subscription()->get();

            if($subscription->count() > 0 && $subscription->last()->reference == Carbon::now()->month){

                return response()->json([
                    'success' => false,
                    'message' => 'Boleto já gerado, deseja receber a segunda via?'
                ]);

            }

            $data['customer'] = Auth::user()->seller_id;
            $data['amount'] = str_replace(".","",number_format($plan->price,2,'.',''));

            $result = (new ZoopService())->createBoletoTransaction($data);

            if($result['success']){
                Provider::find(Auth::user()->id)->subscription()->create([
                    'plan_id' => $request->plan_id,
                    'transaction_id' => $result['transaction_id'],
                    'boleto_id' => $result['boleto_id'],
                    'type' => 'boleto',
                    'reference' => Carbon::now()->month,
                    'started_at' => Carbon::now(),
                    'finished_at' => Carbon::now()->addDays(30),
                    'status' => 0
                ]);

                Auth::user()->notify(new SendSubscriptionMail($result['url']));
            }

            return response()->json([
                'success' => true,
                'message' => 'Solicitação realizada com sucesso!'
            ]);

        } catch (BoletoErrorException $e){

            Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);

        }

    }

    public function cardPay(Request $request)
    {
        try{

            $data = [];

            $plan = Plan::find($request->get('plan_id'));
            $provider = Provider::find(Auth::user()->id);

            $data['customer'] = $provider->seller_id;
            $data['card_id'] = $provider->card()->get()->first()->card_id;
            $data['amount'] = str_replace(".","",number_format($plan->price,2,'.',''));

            $result = (new ZoopService())->createSubscriptionTransaction($data);

            if($result['success']){
                $provider = Provider::find(Auth::user()->id);

                ProviderPaidStatus::dispatch($provider);

                $provider->subscription()->create([
                    'plan_id' => $request->plan_id,
                    'transaction_id' => $result['transaction_id'],
                    'type' => 'card',
                    'reference' => Carbon::now()->month,
                    'started_at' => Carbon::now(),
                    'finished_at' => Carbon::now()->addDays(30),
                    'status' => 1
                ]);

            }

            return response()->json(['message' => 'Solicitação realizada com sucesso!']);

        } catch (ZoopFailedPayment $e){
            Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function boletoRegenerate(Request $request)
    {
        try{

            $data = [];
            $subscription = Provider::find(Auth::user()->id)->subscription()->get()->last();

            if($subscription->type != 'boleto'){

                return response()->json([
                    'success' => false,
                    'message' => 'A forma de pagamento realizada foi cartão de crédito.'
                ]);

            } else {
                $data['transaction_id'] = $subscription->transaction_id;

                $result = (new ZoopService())->regenerateBoleto($data);

                Auth::user()->notify(new SendSubscriptionMail($result['url']));

                return response()->json([
                    'success' => true,
                    'message' => 'Solicitação de segunda via realizada com sucesso!'
                ]);
            }

        }catch (BoletoErrorException $e){
            Log::error($e->getMessage());
            return response()->json([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }


    }

    public function showProviderPlan()
    {
        $provider = Provider::find(Auth::user()->id)->subscription()->with("plan")->get()->last();

        return response()->json($provider);
    }
}
