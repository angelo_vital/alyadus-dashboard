<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProviderPaymentMode
 * @mixin \Eloquent
 */
class ProviderPaymentMode extends Model
{
    protected $fillable = [
        'provider_id', 'cash', 'card', 'machine', 'pix'
    ];
}
