<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProviderPlanTableAddItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_plans', function (Blueprint $table){
            $table->string('reference')->nullable();
            $table->date('started_at')->nullable();
            $table->date('finished_at')->nullable();
            $table->integer('status')->nullable()->default(0)->comment("0: not paid, 1: paid");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
