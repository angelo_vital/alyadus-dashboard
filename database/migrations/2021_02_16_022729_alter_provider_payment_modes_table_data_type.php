<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProviderPaymentModesTableDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('provider_payment_modes', function (Blueprint $table) {
            $table->integer('cash')->nullable()->default(1)->change();
            $table->integer('card')->nullable()->default(0)->change();
            $table->integer('machine')->nullable()->default(0)->change();
            $table->integer('pix')->nullable()->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
