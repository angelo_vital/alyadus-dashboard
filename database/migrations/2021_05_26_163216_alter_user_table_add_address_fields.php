<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTableAddAddressFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("users", function (Blueprint $table){
            $table->string('cep')->nullable()->after('country_code')->default(null);
            $table->string('street')->nullable()->after('cep')->default(null);
            $table->string('number')->nullable()->after('street')->default(null);
            $table->string('complement')->nullable()->after('number')->default(null);
            $table->string('neighborhood')->nullable()->after('complement')->default(null);
            $table->string('city')->nullable()->after('neighborhood')->default(null);
            $table->string('state')->nullable()->after('city')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
