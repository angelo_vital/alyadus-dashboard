<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class PromocodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('promocodes')->truncate();
        DB::table('promocodes')->insert([
            [
                'promo_code' => 'CPD01',
                'percentage' => 10.00,
                'max_amount' => 20.00,
                'promo_description' => '10% off, O desconto máximo é 20',
                'expiration' => Carbon::now()->addMonths(2),
                'status' => 'ADDED',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
