<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class ReasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('reasons')->truncate();
        DB::table('reasons')->insert([
            [
                'type' => 'PROVIDER',
                'reason' => 'Demorar muito tempo para chegar ao ponto de encontro',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'PROVIDER',
                'dispute_name' => 'Tráfego pesado',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'PROVIDER',
                'dispute_name' => 'O usuário informou o local errado',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'PROVIDER',
                'dispute_name' => 'Minha razão não está listada',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'PROVIDER',
                'dispute_name' => 'Usuário indisponível',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'Minha razão não está listada',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'Não é possível entrar em contato com o motorista',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'Esperado muito tempo e o motorista não chegou',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'Os motoristas negaram minha solicitação',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'O motorista negou me levar ao destino',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'dispute_type' => 'USER',
                'dispute_name' => 'O motorista cobrou taxa extra',
                'status' => 'active',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
