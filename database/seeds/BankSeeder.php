<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->delete();
        $json = File::get('database/data/banks.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\Bank::create([
                'code' => $obj->code,
                'name' => $obj->name
            ]);
        }
    }
}
