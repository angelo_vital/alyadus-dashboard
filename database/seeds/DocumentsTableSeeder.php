<?php

use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documents')->truncate();
        DB::table('documents')->insert([
            [
                'name' => 'Comprovante de Residência',
                'type' => 'DRIVER',
            ],
            [
                'name' => 'Conta Bancária (Foto do Cartão ou Extrato)',
                'type' => 'DRIVER',
            ],
            [
                'name' => 'Carteira de Habilitação (CNH)',
                'type' => 'DRIVER',
            ],
            [
                'name' => 'Foto do Veículo',
                'type' => 'VEHICLE',
            ],
            [
                'name' => 'Documento do Veículo (CRV)',
                'type' => 'VEHICLE',
            ],
        ]);
    }
}
