# Funcionalidade UIU homologadas

* Verificando o ambiente de preparo
* Códigos Promocionais
* Carteira
* Viagem agendada
* Tempo Estimado / Preço Estimado
* Pagamento via Zoop Payments
* Preço dinâmico
* Gerenciamento de Franquias
* Gerenciamento de Usuário
* Gerenciamento de Motoristas
* Gerenciamento de Despachante
* Gerenciamento de Conta
* Verificação por Código de confirmação
* Estatísticas / Meta Diária
* O motorista pode cancelar até que o usuário seja pego.
* Sistema de indicação


# Requisitos de Instalação

* Servidor PHP >= 7.2
* MySQL >= 5.7
* Extensão php-gmp
* Tarefas cron (PRODUÇÃO)
* Supervisor (PRODUÇÃO)

# Comandos

* copiar env-example para .env
* composer install
* php artisan key:generate
* php artisan migrate --seed
* php artisan passport:install
* php artisan server
