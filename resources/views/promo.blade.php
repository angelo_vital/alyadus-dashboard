<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <title>UbGo - App de Mobilidade</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="ubgo,promocao,celular" name="keywords">
    <meta content="Promocao UBGO" name="description">

    <!-- Favicons -->
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <link href="{{ asset('favicon.ico') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="{{ asset('lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{ asset('lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/magnific-popup/magnific-popup.css') }}" rel="stylesheet">


    <!-- Main Stylesheet File -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body>

<header id="header">
    <div class="container">

        <div id="logo" class="pull-left">
            <h1><a href="#intro" class="scrollto">UBGo</a></h1>
{{--            <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>--}}
        </div>

        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#intro">Principal</a></li>
                <li><a href="#about">Sobre a Promoção</a></li>
                <li><a href="#features">Como Participar</a></li>
            </ul>
        </nav><!-- #nav-menu-container -->
    </div>
</header><!-- #header -->

<section id="intro">

    <div class="intro-text">
        <h2>Promoção de Aniversário UBGo!</h2>
        <p>A UBGo está fazendo aniversário, mas quem ganha o presente é o nosso cliente!</p>
        <a href="#about" class="btn-get-started scrollto">Saiba mais</a>
    </div>

    <div class="product-screens">

        <div class="product-screen-1" data-aos="fade-up" data-aos-delay="400">
            <img src="img/product-screen-1.png" alt="">
        </div>

        <div class="product-screen-2" data-aos="fade-up" data-aos-delay="200">
            <img src="img/product-screen-2.png" alt="">
        </div>

        <div class="product-screen-3" data-aos="fade-up">
            <img src="img/product-screen-3.png" alt="">
        </div>

    </div>

</section><!-- #intro -->

<main id="main">
    <section id="about" class="section-bg">
        <div class="container-fluid">
            <div class="section-header">
                <h3 class="section-title">Sobre a Promoção</h3>
                <span class="section-divider"></span>
                <p class="section-description">
                    A UBGo está completando 1 ano em Cruzeiro do Sul, e em comemoração a esta data
                    estaremos sorteando a nossos clientes um Celular Novinho e um Vale Compras no valor de R$ 300,00
                </p>
                <p>
                    A UBGo está sempre em busca de melhorias para que nossos clientes sejam atendimentos com excelencia.
                </p>

                <p>Baixe agora mesmo o nosso aplicativo.</p>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <a href="https://play.google.com/store/apps/details?id=br.com.ubgo.passageiro" class="btn btn-primary">App Android</a>
                    <a href="https://itunes.apple.com/br/app/ubgo/id1462819765?mt=8" class="btn btn-primary">App iOS</a>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-lg-6 about-img" data-aos="fade-right">
                    <img src="{{ asset('img/premio.png') }}" alt="">
                </div>
            </div>

        </div>
    </section><!-- #about -->
    <section id="features">
        <div class="container">

            <div class="row">

                <div class="col-lg-8 offset-lg-4">
                    <div class="section-header" data-aos="fade" data-aos-duration="1000">
                        <h3 class="section-title">Como Participar</h3>
                        <span class="section-divider"></span>
                    </div>
                </div>

                <div class="col-lg-4 col-md-5 features-img">
                    <img src="img/product-features.png" alt="" data-aos="fade-right" data-aos-easing="ease-out-back">
                </div>

                <div class="col-lg-8 col-md-7 content">

                    <ul>
                        <li><i class="ion-android-checkmark-circle"></i> A cada viagem realizada no aplicativo, voce já estará concorrendo.</li>
                        <li><i class="ion-android-checkmark-circle"></i> Será encaminhado automaticamente um email com um número de cupom para sorteio, guarde esse email, pois ele é importante para que voce senha o vencedor</li>
                        <li><i class="ion-android-checkmark-circle"></i> Quanto mais voce viajar, mais chances terá de ganhar!!.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!-- #features -->
</main>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 text-lg-left text-center">
                <div class="copyright">
                    &copy; Copyright <strong>UBGo</strong>. Todos os direitos reservados
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
                    <a href="#intro" class="scrollto">Principal</a>
                    <a href="#about" class="scrollto">Sobre a Promoção</a>
                </nav>
            </div>
        </div>
    </div>
</footer><!-- #footer -->

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

<!-- JavaScript Libraries -->
<script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('lib/jquery/jquery-migrate.min.js') }}"></script>
<script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('lib/easing/easing.min.js') }}"></script>
<script src="{{ asset('lib/aos/aos.js') }}"></script>
<script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
<script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
<script src="{{ asset('lib/magnific-popup/magnific-popup.min.js') }}"></script>

<!-- Template Main Javascript File -->
<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>
