<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>UbGo - App de Mobilidade</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <style>
        body {
            background-image: url('asset/img/bg.jpg');
            width: 100%;
            height: 100%;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
        #fundo {
            background-image: url('asset/img/topo.png');
            background-position: center;
            background-repeat: no-repeat;
        }
        #logos {
            width: 100%;
        }
        #celular {
            width: 100%;
        }
        .card {
            background-color: transparent;
        }
        .btn-warning{
            color: #fff;
            background-color: #0d5abd;
            border-color: #fff;
        }
        @media only screen and (min-width: 600px) {
            #meio {
                margin: 12% 0 0 0;
            }
            #celular {
                margin: 50% 0 0 0;
            }
            #logos {
                margin: 100% 0 0 25%;
                width: 85%;
            }
            .navbar {
                background-color: rgba(0, 0, 0, 0) !important;
                background-image: url("asset/img/fundotopo.png");
            }
        }
        @media only screen and (max-width: 601px) {
            #meio {
                margin: 8% 0 8% 0;
            }
            #meio img{
                width:150px;
            }
            #celular, #logos {
                margin: 20% 0 0 25%;
                width: 50%;
            }
            .navbar {
                background-color: rgba(0, 0, 0, 0) !important;
                background-image: url("asset/img/fundotopomobile.png");
                background-repeat: repeat-x;
            }
        }
    </style>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '502998940325335');
        fbq('track', 'PageView');
        fbq('track', 'StartTrial');
        document.getElementById('startTrialButton').addEventListener('click', function() {
         console.log("Baixou")
        }, false);
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=502998940325335&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<nav id="menutopo" class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#"></a>
    <a class="navbar-toggler" href="{{ url('/') }}" style="background-color: #000000" data-toggle="collapse" data-target="#navbarCollapse"
       aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="text-white btn btn-outline-primary my-2 my-sm-0" href="{{ url('/') }}">Inicio <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="text-white btn btn-outline-primary my-2 my-sm-0" href="{{ url('register') }}">Registre-se Para Viajar</a>
            </li>
            <li class="nav-item active">
                <a class="text-white btn btn-outline-primary my-2 my-sm-0" href="{{ url('provider/register') }}">Seja um Motorista</a>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0">
            <a href="{{ url('login') }}" class="text-white btn btn-outline-primary my-2 my-sm-0">Entrar</a>
        </form>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <img src="{{ asset('asset/img/logos.png') }}" id="logos">
        </div>
        <div id="meio" class="col-sm-8 text-center">
            <h1 class="display-4 font-weight-normal text-white">UBGo</h1>
            <p class="lead font-weight-normal text-white">EXPERIMENTE O APLICATIVO</p>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3 col-xs-3" style="text-align: right;">
                    <a id="StartTrial" href="https://play.google.com/store/apps/details?id=br.com.ubgo.passageiro">
                        <img src="{{ asset('asset/img/passageiro_android.png') }}" width="190" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-3" style="text-align: left;">
                    <a href="https://play.google.com/store/apps/details?id=br.com.ubgo.motorista">
                        <img src="{{ asset('asset/img/motorista_android.png') }}" width="190" alt="">
                    </a>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3 col-xs-3" style="text-align: right;">
                    <a id="ios" href="https://itunes.apple.com/br/app/ubgo/id1462819765?mt=8">
                        <img src="{{ asset('asset/img/passageiro_apple.png') }}" width="190" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-xs-3" style="text-align: left;">
                    <a href="#">
                        <img src="{{ asset('asset/img/motorista_apple.png') }}" width="190" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-2"><img src="{{ asset('asset/img/celular.png') }}" id="celular"></div>

    </div>
</div>
</body>
</html>
