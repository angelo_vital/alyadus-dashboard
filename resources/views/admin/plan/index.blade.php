@extends('admin.layout.base')

@section('title', 'Planos ')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">Planos de assinatura</h5>
                <a href="{{ route('admin.plan.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Novo</a>

                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome </th>
                        <th>Preço </th>
                        <th>Período </th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($plans as $plan)
                        <tr>
                            <td>{{ $plan->id }}</td>
                            <td>{{ $plan->name }}</td>
                            <td>{{ number_format($plan->price, 2, ',', '.') }}</td>
                            <td>{{ $plan->period }}</td>
                            <td>
                                <form action="{{ route('admin.plan.destroy', $plan->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <a href="{{ route('admin.plan.edit', $plan->id) }}" class="btn btn-info" title="Editar"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger" onclick="return confirm('Você tem certeza?')"><i class="fa fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nome </th>
                        <th>Preço </th>
                        <th>Período </th>
                        <th>Ação</th>
                    </tr>
                    </tfoot>
                </table>
            </div>

        </div>
    </div>
@endsection