@extends('admin.layout.base')

@section('title', 'Plano ')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <a href="{{ route('admin.plan.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Voltar</a>

                <h5 style="margin-bottom: 2em;">Cadastrar plano</h5>

                <form class="form-horizontal" action="{{route('admin.plan.update', $plan->id)}}" method="POST" enctype="multipart/form-data" role="form">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group row">
                        <label for="reason" class="col-xs-2 col-form-label">Nome</label>
                        <div class="col-xs-10">
                            <input class="form-control" autocomplete="off"  type="text" value="{{ $plan->name }}" name="name" required id="name" placeholder="Nome">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reason" class="col-xs-2 col-form-label">Preço</label>
                        <div class="col-xs-10">
                            <input class="form-control" autocomplete="off" id="currency_price" data-thousands="." data-decimal="," type="text" value="{{ $plan->price }}" name="price" required id="price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="reason" class="col-xs-2 col-form-label">Período</label>
                        <div class="col-xs-10">
                            <input class="form-control" autocomplete="off"  type="number" value="{{ $plan->period }}" name="period" required id="period">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="zipcode" class="col-xs-2 col-form-label"></label>
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                            <a href="{{route('admin.plan.index')}}" class="btn btn-default">Cancelar</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

