@extends('admin.layout.base')

@section('title', 'Ajuda ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                Para quaisquer informações, entre em contato com o desenvolvedor.
                <br><br>
                contato@creativemobile.com.br
            </div>
        </div>
    </div>

@endsection
