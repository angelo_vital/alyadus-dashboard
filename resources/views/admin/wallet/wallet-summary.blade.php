<div class="col-md-4">
    <div class="box bg-white">
        <div class="box-block clearfix">
            <h5 class="float-xs-left">@lang('admin.dashboard.wallet_summary')</h5>
            <div class="float-xs-right">
            </div>
        </div>
        <table class="table mb-md-0">
            <tbody>
            @php($total=$wallet['admin'])
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_admin_credit')</th>
                <td class="text-success">{{currency($wallet['admin'])}}</td>
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_provider_credit')</th>
                @if($wallet['provider_credit'])
                    @php($total=$total-$wallet['provider_credit'][0]['total_credit'])
                    <td class="text-success">{{currency($wallet['provider_credit'][0]['total_credit'])}}</td>
                @else
                    <td class="text-success">{{currency()}}</td>
                @endif
            </tr>

            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_provider_debit')</th>
                @if($wallet['provider_debit'])

                    <td class="text-danger">{{currency($wallet['provider_debit'][0]['total_debit'])}}</td>
                @else
                    <td class="text-danger">{{currency()}}</td>
                @endif
            </tr>

            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_fleet_credit')</th>
                @if($wallet['fleet_credit'])
                    @php($total=$total-($wallet['fleet_credit'][0]['total_credit']))
                    <td class="text-success">{{currency($wallet['fleet_credit'][0]['total_credit'])}}</td>
                @else
                    <td class="text-success">{{currency()}}</td>
                @endif
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_fleet_debit')</th>
                @if($wallet['fleet_debit'])
                    <td class="text-danger">{{currency($wallet['fleet_debit'][0]['total_debit'])}}</td>
                @else
                    <td class="text-danger">{{currency()}}</td>
                @endif
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_commission')</th>
                <td class="text-success">{{currency($wallet['admin_commission'])}}</td>
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_peak_commission')</th>
                <td class="text-success">{{currency($wallet['peak_commission'])}}</td>
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_waitining_commission')</th>
                <td class="text-success">{{currency($wallet['waiting_commission'])}}</td>
            </tr>
            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_discount')</th>
                <td class="text-danger">{{currency($wallet['admin_discount'])}}</td>
            </tr>
            <tr>
                @php($total=$total-($wallet['admin_tax']))
                <th scope="row">@lang('admin.dashboard.wallet_summary_tax_amount')</th>
                <td class="text-success">{{currency($wallet['admin_tax'])}}</td>
            </tr>

            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_tips')</th>
                <td class="text-danger">{{currency($wallet['tips'])}}</td>
            </tr>

            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_referrals')</th>
                <td class="text-danger">{{currency($wallet['admin_referral'])}}</td>
            </tr>

            <tr>
                <th scope="row">@lang('admin.dashboard.wallet_summary_disputes')</th>
                <td class="text-danger">{{currency($wallet['admin_dispute'])}}</td>
            </tr>

            <!--                             <tr>
                            <th scope="row text-right">@lang('admin.dashboard.wallet_summary_total')</th>
                            <td>{{currency($total)}}</td>
                        </tr> -->
            </tbody>
        </table>
    </div>
</div>