@extends('admin.layout.base')

@section('title', 'Detalhes da Viagem ')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <div class="row">
                    <div class="col-md-6">
                        <h4>@lang('admin.request.request_details')</h4>
                    </div>
                    <div class="col-md-6 right" style="display: inline-flex">
                        @if($request->status === 'PICKEDUP' || $request->status === 'DROPPED')
                            <form id="complete_request" method="POST"
                                  action="{{route('admin.requests.complete', [$request->id])}}">
                                {{ csrf_field() }}
                                <button class="btn btn-primary" type="button" onclick="completeRequest()">
                                    <span class="fa fa-check-circle"></span> Completar
                                </button>
                            </form>
                        @endif

                        @if($request->status != 'COMPLETED' && $request->status != 'CANCELLED' )
                            <form id="cancel_request" method="POST"
                                  action="{{route('admin.requests.cancel', [$request->id])}}" onsubmit="return confirm('Deseja realmente cancelar essa viagem?');">
                                {{ csrf_field() }}
                                <button class="btn btn-warning" type="submit">
                                    <span class="fa fa-minus-square"></span> Cancelar
                                </button>
                            </form>
                        @endif
                        <form id="delete_request"
                              action="{{ route('admin.requests.delete', $request->id) }}"
                              method="POST">
                            {{ csrf_field() }}
                            @if( Setting::get('demo_mode', 0) == 0)
                                {{ method_field('DELETE') }}
                                @can('ride-delete')
                                    <button class="btn btn-danger" type="button" onclick="deleteRequest()">
                                        <span class="fa fa-trash"></span> Excluir
                                    </button>
                                @endcan
                            @endif
                        </form>
                        <div style="flex: 1"></div>
                        @if(isset($request->provider->id))
                            <button type="button" class="btn btn-success" onclick="refreshMap()">
                                <span class="fa fa-refresh"></span> Atualizar mapa
                            </button>
                        @endif
                    </div>
                </div>
                <a href="{{ route('admin.requests.index') }}" class="btn btn-default -align-right">
                    <i class="fa fa-angle-left"></i> @lang('admin.back')
                </a>
                <div class="row">
                    <div class="col-md-6">
                        <dl class="row">
                            <dt class="col-sm-4">@lang('admin.request.Booking_ID')</dt>
                            <dd class="col-sm-8">{{ $request->booking_id }}</dd>

                            <dt class="col-sm-4">@lang('admin.request.Scheduled_Date_Time')</dt>
                            <dd class="col-sm-8">{{strftime('%a, %d de %b de %Y às %H:%M', strtotime($request->created_at))}}</dd>

                            <dt class="col-sm-4">@lang('admin.request.Booking_ID')</dt>
                            <dd class="col-sm-8">{{ $request->booking_id }}</dd>

                            <dt class="col-sm-4">@lang('admin.request.User_Name')</dt>
                            <dd class="col-sm-8">
                                <a class="text-primary" href="{{route('admin.user.show',$request->user->id)}}">
                                    <span class="fa fa-search"></span>
                                    {{ $request->user->first_name }}
                                </a>
                            </dd>
                            <dt class="col-sm-4">@lang('admin.request.Provider_Name')</dt>
                            @if($request->provider)
                                <dd class="col-sm-8">
                                    <a class="text-primary"
                                       href="{{route('admin.provider.show',$request->provider->id)}}">
                                        <span class="fa fa-search"></span>
                                        {{ $request->provider->first_name }}
                                    </a>
                                </dd>
                            @else
                                <dd class="col-sm-8">@lang('admin.request.provider_not_assigned')</dd>
                            @endif

                            <dt class="col-sm-4">@lang('admin.request.total_distance')</dt>
                            <dd class="col-sm-8">{{ $request->distance ? $request->distance : '-' }}{{$request->unit}}</dd>

                            @if($request->status == 'SCHEDULED')
                                <dt class="col-sm-4">@lang('admin.request.ride_scheduled_time')</dt>
                                <dd class="col-sm-8">
                                    @if($request->schedule_at != "")
                                        {{ strftime('%A, %d de %B de %Y', strtotime($request->schedule_at)) }}
                                    @else
                                        -
                                    @endif
                                </dd>
                            @else
                                <dt class="col-sm-4">@lang('admin.request.ride_start_time')</dt>
                                <dd class="col-sm-8">
                                    @if($request->started_at != "")
                                        {{ strftime('%A, %d de %B de %Y', strtotime($request->started_at)) }}
                                    @else
                                        -
                                    @endif
                                </dd>

                                <dt class="col-sm-4">@lang('admin.request.ride_end_time')</dt>
                                <dd class="col-sm-8">
                                    @if($request->finished_at != "")
                                        {{ strftime('%A, %d de %B de %Y', strtotime($request->finished_at)) }}
                                    @else
                                        -
                                    @endif
                                </dd>
                            @endif

                            <dt class="col-sm-4">@lang('admin.request.pickup_address')</dt>
                            <dd class="col-sm-8">{{ $request->s_address ? $request->s_address : '-' }}</dd>

                            <dt class="col-sm-4">@lang('admin.request.drop_address')</dt>
                            <dd class="col-sm-8">{{ $request->d_address ? $request->d_address : '-' }}</dd>

                            @if($request->payment)
                                <dt class="col-sm-4">@lang('admin.request.base_price')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->fixed) }}</dd>
                                @if($request->service_type->calculator=='MIN')
                                    <dt class="col-sm-4">@lang('admin.request.minutes_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->minute) }}</dd>
                                @endif
                                @if($request->service_type->calculator=='HOUR')
                                    <dt class="col-sm-4">@lang('admin.request.hours_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->hour) }}</dd>
                                @endif
                                @if($request->service_type->calculator=='DISTANCE')
                                    <dt class="col-sm-4">@lang('admin.request.distance_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->distance) }}</dd>
                                @endif
                                @if($request->service_type->calculator=='DISTANCEMIN')
                                    <dt class="col-sm-4">@lang('admin.request.minutes_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->minute) }}</dd>
                                    <dt class="col-sm-4">@lang('admin.request.distance_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->distance) }}</dd>
                                @endif
                                @if($request->service_type->calculator=='DISTANCEHOUR')
                                    <dt class="col-sm-4">@lang('admin.request.hours_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->hour) }}</dd>
                                    <dt class="col-sm-4">@lang('admin.request.distance_price')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->distance) }}</dd>
                                @endif
                                <dt class="col-sm-4">@lang('admin.request.commission')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->commision) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.fleet_commission')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->fleet) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.discount_price')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->discount) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.peak_amount')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->peak_amount) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.peak_commission')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->peak_comm_amount) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.waiting_charge')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->waiting_amount) }}</dd>

                                <dt class="col-sm-4"
                                    style="padding-right:0px;">@lang('admin.request.waiting_commission')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->waiting_comm_amount) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.tax_price')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->tax) }}</dd>

                            <!--  <dt class="col-sm-4">@lang('admin.request.surge_price') :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->surge) }}</dd> -->

                                <dt class="col-sm-4">@lang('admin.request.tips')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->tips) }}</dd>

                                <dt class="col-sm-4">@lang('user.ride.round_off')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->round_of) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.total_amount')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->total+$request->payment->tips) }}</dd>

                                <dt class="col-sm-4">@lang('admin.request.wallet_deduction')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->wallet) }}</dd>

                            <!-- <dt class="col-sm-4">@lang('admin.request.paid_amount') :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->payable) }}</dd> -->

                                <dt class="col-sm-4">@lang('admin.request.payment_mode')</dt>
                                @if($request->payment->payment_mode == "CASH")
                                    <dd class="col-sm-8">DINHEIRO</dd>
                                @elseif($request->payment->payment_mode == "CARD")
                                    <dd class="col-sm-8">CARTÃO</dd>
                                @elseif($request->payment->payment_mode == "MACHINE")
                                    <dd class="col-sm-8">MAQUINETA</dd>
                                @else
                                    {{ $request->payment->payment_mode }}
                                @endif
                                @if($request->payment->payment_mode=='CASH')
                                    <dt class="col-sm-4">@lang('admin.request.cash_amount')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->cash) }}</dd>
                                @else
                                    <dt class="col-sm-4">@lang('admin.request.card_amount')</dt>
                                    <dd class="col-sm-8">{{ currency($request->payment->card) }}</dd>
                                @endif
                                <dt class="col-sm-4">@lang('admin.request.provider_earnings')</dt>
                                <dd class="col-sm-8">{{ currency($request->payment->provider_pay - $request->payment->discount) }}</dd>

                            <!--  <dt class="col-sm-4">Provider Admin Commission :</dt>
                        <dd class="col-sm-8">{{ currency($request->payment->provider_commission) }}</dd> -->
                            @endif
                            <dt class="col-sm-4">@lang('admin.request.estimated_price')</dt>
                            <dd class="col-sm-8">{{ currency($request->estimated_fare) }}</dd>
                            <dt class="col-sm-4">@lang('admin.request.distance')</dt>
                            <dd class="col-sm-8">{{ $request->distance }}{{$request->unit}}</dd>
                            <dt class="col-sm-4">@lang('admin.request.track_distance')</dt>
                            <dd class="col-sm-8">{{ $request->distance_accepted }}</dd>
                            <dt class="col-sm-4">@lang('admin.request.ride_status')</dt>
                            <dd class="col-sm-8">
                                @if($request->status == "COMPLETED")
                                    <span class="tag tag-success">CONCLUÍDA</span>
                                @elseif($request->status == "CANCELLED")
                                    <span class="tag tag-danger">CANCELADA</span>
                                @elseif($request->status == "ARRIVED")
                                    <span class="tag tag-info">EM ANDAMENTO</span>
                                @elseif($request->status == "SEARCHING")
                                    <span class="tag tag-info">PESQUISANDO</span>
                                @elseif($request->status == "ACCEPTED")
                                    <span class="tag tag-info">MOTORISTA A CAMINHO</span>
                                @elseif($request->status == "STARTED")
                                    <span class="tag tag-info">VIAGEM ACEITA</span>
                                @elseif($request->status == "DROPPED")
                                    <span class="tag tag-purple">NO DESTINO</span>
                                @elseif($request->status == "PICKEDUP")
                                    <span class="tag tag-info">INICIADA</span>
                                @elseif($request->status == "SCHEDULED")
                                    <span class="tag tag-info">AGENDADA</span>
                                @endif
                                <br/> {{ $request->cancel_reason }}
                            </dd>
                            @if($request->status == "CANCELLED")
                                <dt class="col-sm-4">Cancelado por:</dt>
                                <dd class="col-sm-8">
                                    @if($request->cancelled_by == "USER")
                                        USUÁRIO
                                    @elseif($request->cancelled_by == "PROVIDER")
                                        MOTORISTA
                                    @endif
                                </dd>
                            @endif
                            @if($request->status =="COMPLETED")
                                <dt class="col-sm-4">@lang('admin.request.user_rating')</dt>
                                <dd class="col-sm-8">
                                    @if($request->user_rated ==1)
                                        {{ $request->rating->user_rating}}
                                    @else
                                        -
                                    @endif
                                </dd>
                                <dt class="col-sm-4">@lang('admin.request.user_comment')</dt>
                                <dd class="col-sm-8">
                                    @if($request->user_rated ==1)
                                        {{ $request->rating->user_comment }}
                                    @else
                                        -
                                    @endif
                                </dd>
                                <dt class="col-sm-4">@lang('admin.request.provider_rating')</dt>
                                <dd class="col-sm-8">
                                    @if($request->provider_rated ==1)
                                        {{ $request->rating->provider_rating }}
                                    @else
                                        -
                                    @endif
                                </dd>
                                <dt class="col-sm-4">@lang('admin.request.provider_comment')</dt>
                                <dd class="col-sm-8">
                                    @if($request->provider_rated ==1)
                                        {{ $request->rating->provider_comment }}
                                    @else
                                        -
                                    @endif
                                </dd>
                            @endif
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <style type="text/css">
        #map {
            height: 450px;
        }
    </style>
@endsection

@section('scripts')
    <script type="text/javascript">
        var map;
        var zoomLevel = 11;
        var departure = {
            latitude: "{{ $request->s_latitude }}",
            longitude: "{{ $request->s_longitude }}"
        };
        var arrival = {
            latitude: "{{ $request->d_latitude }}",
            longitude: "{{ $request->d_longitude }}"
        };

        var providerPos = {
            latitude: "{{ isset($request->provider->latitude) ? $request->provider->latitude: 0 }}",
            longitude: "{{ isset($request->provider->longitude) ? $request->provider->longitude : 0 }}"
        };

        function completeRequest() {
            if (confirm('Deseja realmente completar essa viagem?')) {
                document.getElementById('complete_request').submit();
            }
        }

        function cancelRequest() {
            if (confirm('Deseja realmente cancelar essa viagem?')) {
                document.getElementById('cancel_request').submit();
            }
        }

        function deleteRequest() {
            if (confirm('Deseja realmente excluir essa viagem?')) {
                document.getElementById('delete_request').submit();
            }
        }

        function refreshMap() {
            $.ajax({
                url: '/admin/provider/{{ isset($request->provider->id) ? $request->provider->id : 0 }}/get-location',
                success: function (data) {
                    providerPos.latitude = data.latitude;
                    providerPos.longitude = data.longitude;
                    initMap();
                }
            });
        }

        function initMap() {

            map = new google.maps.Map(document.getElementById('map'));

            var marker = new google.maps.Marker({
                map: map,
                icon: '/asset/img/marker-start.png',
                anchorPoint: new google.maps.Point(0, -29)
            });

            var markerSecond = new google.maps.Marker({
                map: map,
                icon: '/asset/img/marker-end.png',
                anchorPoint: new google.maps.Point(0, -29)
            });

            var bounds = new google.maps.LatLngBounds();

            source = new google.maps.LatLng(departure.latitude, departure.longitude);
            destination = new google.maps.LatLng(arrival.latitude, arrival.longitude);

            marker.setPosition(source);
            markerSecond.setPosition(destination);

            var encoded = "{{ $request->route_key }}";

            var decode = google.maps.geometry.encoding.decodePath(encoded);

            var poly = new google.maps.Polyline({
                path: decode,
                strokeColor: "#000000",
                strokeWeight: 4,
                strokeOpacity: 0.5,
            });

            poly.setMap(map);

            /*var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
            directionsDisplay.setMap(map);

            directionsService.route({
                origin: source,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING
            }, function (result, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    console.log(result);
                    directionsDisplay.setDirections(result);

                    marker.setPosition(result.routes[0].legs[0].start_location);
                    markerSecond.setPosition(result.routes[0].legs[0].end_location);
                }
            });*/

                    @if($request->provider && $request->status != 'COMPLETED')
            var markerProvider = new google.maps.Marker({
                    map: map,
                    icon: "/asset/img/marker-car.png",
                    anchorPoint: new google.maps.Point(0, -29)
                });

            provider = new google.maps.LatLng(providerPos.latitude, providerPos.longitude);
            markerProvider.setVisible(true);
            markerProvider.setPosition(provider);
            console.log('Provider Bounds', markerProvider.getPosition());
            bounds.extend(markerProvider.getPosition());
            @endif

            bounds.extend(marker.getPosition());
            bounds.extend(markerSecond.getPosition());
            map.fitBounds(bounds);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ Config::get('constants.map_key') }}&libraries=geometry&callback=initMap"
            async defer></script>
@endsection
