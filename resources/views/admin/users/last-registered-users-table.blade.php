<div class="box bg-white">
    <div class="box-block clearfix">
        <h5 class="float-xs-left">@lang('admin.dashboard.last-registered-users')</h5>
        <div class="float-xs-right">
        </div>
    </div>
    <table class="table mb-md-0">
        <tbody>
        <tr>
            <th>@lang('admin.id')</th>
            <th>@lang('admin.provides.full_name')</th>
            <th>@lang('admin.city')</th>
        </tr>
        @foreach($last_users as $index => $user)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$user->first_name}} {{$user->last_name}}</td>
                <td>{{$user->city ? $user->city->title : 'S/ cidade'}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>