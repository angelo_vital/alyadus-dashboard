@extends('admin.layout.base')

@section('title', 'Passageiros ')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">
                    <p>
                        @lang('admin.users.Users')
                    </p>
                    <p>
                    <form action="{{ route('admin.user.index') }}" method="get">
                        <div class="input-group mb-3">
                            <input name="name" type="text" class="form-control"
                                   placeholder="Nome do Passageiro ou Email" aria-label="Nome do Passageiro"
                                   aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Pesquisar</button>
                            </div>
                        </div>
                    </form>
                    </p>
                </h5>
                @can('user-create')
                    <a href="{{ route('admin.user.create') }}" style="margin-left: 1em;"
                       class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Adicionar Novo</a>
                @endcan
                <table class="table table-striped table-bordered dataTable" id="table-5">
                    <thead>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.first_name')</th>
                        <th>@lang('admin.last_name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.users.Rating')</th>
                        <th>@lang('admin.users.Wallet_Amount')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @php($page = ($pagination->currentPage-1)*$pagination->perPage)
                    @foreach($users as $index => $user)
                        @php($page++)
                        <tr>
                            <td>{{ $page }}</td>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            @if(Setting::get('demo_mode', 0) == 1)
                                <td>{{ substr($user->email, 0, 3).'****'.substr($user->email, strpos($user->email, "@")) }}</td>
                            @else
                                <td>{{ $user->email }}</td>
                            @endif
                            @if(Setting::get('demo_mode', 0) == 1)
                                <td>+919876543210</td>
                            @else
                                <td>{{ $user->mobile }}</td>
                            @endif
                            <td>{{ $user->rating }}</td>
                            <td>{{currency($user->wallet_balance)}}</td>
                            <td>
                                <form action="{{ route('admin.user.destroy', $user->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    @can('user-history')
                                        <a href="{{ route('admin.user.request', $user->id) }}" class="btn btn-info"><i
                                                    class="fa fa-search"></i> @lang('admin.History')</a>
                                    @endcan
                                    @if( Setting::get('demo_mode', 0) == 0)

                                        @can('user-create')
                                            <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-info"><i
                                                        class="fa fa-pencil"></i> @lang('admin.edit')</a>
                                        @endcan

                                        @can('user-delete')
                                            <button class="btn btn-danger"
                                                    onclick="return confirm('Você tem certeza?')"><i
                                                        class="fa fa-trash"></i> @lang('admin.delete')</button>
                                        @endcan

                                    @endif
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.first_name')</th>
                        <th>@lang('admin.last_name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.users.Rating')</th>
                        <th>@lang('admin.users.Wallet_Amount')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                    </tfoot>
                </table>
                @include('common.pagination')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery.fn.DataTable.Api.register('buttons.exportData()', function (options) {
            if (this.context.length) {
                var jsonResult = $.ajax({
                    url: "{{url('admin/user')}}?page=all",
                    data: {},
                    success: function (result) {
                        p = new Array();
                        $.each(result.data, function (i, d) {
                            var item = [d.id, d.first_name, d.last_name, d.email, d.mobile, d.rating, d.wallet_balance];
                            p.push(item);
                        });
                    },
                    async: false
                });
                var head = new Array();
                head.push("ID", "First Name", "Last Name", "Email", "Mobile", "Rating", "Wallet Amount");
                return {body: p, header: head};
            }
        });

        $('#table-5').DataTable({
            responsive: true,
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            },
            paging: false,
            info: false,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ]
        });
    </script>
@endsection
