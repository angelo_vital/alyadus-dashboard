@extends('admin.layout.base')

@section('title','Documentos do motorista')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">

            <div class="box box-block bg-white">
                <h5 class="mb-1">@lang('admin.provides.provider_name'): {{ $provider->first_name }} {{ $provider->last_name }}</h5>
                <h5 class="mb-1">@lang('admin.document.documents')</h5>


                <div class="row">

                    <form class="form-horizontal" action="{{ route('admin.provider.document.store', [$provider->id]) }}" method="POST" enctype="multipart/form-data" role="form" autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" id="type" name="type" value="documents">
                        @foreach($documents as $document)
                            <div class="form-group row">
                                <label for="{{'document'.$document->id}}" class="col-xs-3 col-md-2 col-form-label ml-2">{{ $document->name }}</label>
                                <div class="col-xs-9">
                                    <input type="file" accept="image/*" name="{{'document'.$document->id}}" class="dropify form-control-file" id="site_logo" aria-describedby="fileHelp">
                                </div>
                            </div>
                        @endforeach

                        <div class="form-group row">
                            <div class="col-xs-6">
                                <button class="btn btn-block btn-success" type="submit">@lang('admin.send')</button>
                            </div>
                            <div class="col-xs-6">
                                <a href="{{ route('admin.provider.document.index',$provider->id) }}" class="btn btn-block btn-danger" >@lang('admin.cancel')</a>
                            </div>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>
@endsection
