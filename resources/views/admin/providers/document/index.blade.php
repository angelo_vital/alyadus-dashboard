@extends('admin.layout.base')

@section('title', 'Documentos do Motorista ')

@section('content')
    <div class="content-area py-1">
        <div class="container-fluid">
            @can('provider-services')
                <div class="box box-block bg-white">
                    <h5 class="mb-1">
                        <p>@lang('admin.provides.type_allocation')</p>
                        <p>Nome do Motorista: {{ $Provider->first_name }} {{ $Provider->last_name }} </p>
                    </h5>
                    <a href="{{$backurl}}" style="margin-left: 1em;margin-top: -30px"
                       class="btn btn-primary pull-right"><i class="fa fa-arrow-left"></i> @lang('admin.back')</a>
                    <div class="row">
                        <div class="col-xs-12">
                            @if($ProviderService->count() > 0)
                                <hr><h6>Serviço Atribuído: </h6>
                                <table class="table table-striped table-bordered dataTable">
                                    <thead>
                                    <tr>
                                        <th>@lang('admin.provides.service_name')</th>
                                        <th>@lang('admin.provides.service_number')</th>
                                        <th>@lang('admin.provides.service_model')</th>
                                        <th>@lang('admin.action')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($ProviderService as $service)
                                        <tr>
                                            <td>{{ $service->service_type->name }}</td>
                                            <td>{{ $service->service_number }}</td>
                                            <td>{{ $service->service_model }}</td>
                                            <td>
                                                @if( Setting::get('demo_mode', 0) == 0)
                                                    <form action="{{ route('admin.provider.document.service', [$Provider->id, $service->id]) }}"
                                                          method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}
                                                        @can('provider-service-delete')
                                                            <button class="btn btn-danger btn-large btn-block">@lang('admin.delete')</a>@endcan
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>@lang('admin.provides.service_name')</th>
                                        <th>@lang('admin.provides.service_number')</th>
                                        <th>@lang('admin.provides.service_model')</th>
                                        <th>@lang('admin.action')</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @endif
                            <hr>
                        </div>
                        <form action="{{ route('admin.provider.document.store', $Provider->id) }}" method="POST">
                            {{ csrf_field() }}
                            <div class="col-xs-3">
                                <select class="form-control input" name="service_type" required>
                                    @forelse($ServiceTypes as $Type)
                                        <option value="{{ $Type->id }}">{{ $Type->fleet->name }} - {{ $Type->name }}</option>
                                    @empty
                                        <option>- @lang('admin.service_select') -</option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <input type="text" required name="service_number" class="form-control"
                                       placeholder="Número/Placa (jss-0000)">
                            </div>
                            <div class="col-xs-3">
                                <input type="text" required name="service_model" class="form-control"
                                       placeholder="Modelo (Siena Sedan - Branco)">
                            </div>
                            @if( Setting::get('demo_mode', 0) == 0)
                                <div class="col-xs-3">
                                    @can('provider-service-update')
                                        <button class="btn btn-primary btn-block"
                                                type="submit">@lang('admin.update')</button>@endcan
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            @endcan
            @can('provider-documents')
                <div class="box box-block bg-white">
                    <h5 class="mb-1">
                        @lang('admin.provides.provider_documents')<br>
                    </h5>
                    @if(is_null($Provider->documents) || empty($Provider->documents) || $documentTypes != count($Provider->documents))
                        <div class="col-xs-12 col-md-12 mb-1">
                            <a href="{{ route('admin.provider.document.create',$Provider->id) }}"  class="btn btn-primary pull-right">
                                <i class="fa fa-add"></i>
                                Enviar Documento </a>
                        </div>
                    @endif
                    @if( Setting::get('demo_mode', 0) == 0)
                        @if(count($Provider->documents)>0)
                            <a href="{{route('admin.download', $Provider->id)}}" style="margin-left: 1em;"
                               class="btn btn-primary pull-right"><i
                                        class="fa fa-download"></i> @lang('admin.provides.download')</a>
                        @endif
                    @endif
                    <table class="table table-striped table-bordered dataTable" id="table-2">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('admin.provides.document_type')</th>
                            <th>@lang('admin.status')</th>
                            <th>@lang('admin.action')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Provider->documents as $Index => $Document)
                            <tr>
                                <td>{{ $Index + 1 }}</td>
                                <td>@if($Document->document){{ $Document->document->name }}@endif</td>
                                <td>@if($Document->status == "ACTIVE"){{ "APROVADO" }}@else {{ "AVALIAÇÃO PENDENTE" }} @endif</td>
                                <td>
                                    <div class="input-group-btn">
                                        @if( Setting::get('demo_mode', 0) == 0)
                                            @can('provider-document-edit')
                                                <a href="{{ route('admin.provider.document.edit', [$Provider->id, $Document->id]) }}"><span
                                                            class="btn btn-success btn-large">@lang('admin.view')</span></a>
                                            @endcan
                                            @can('provider-document-delete')
                                                <button class="btn btn-danger btn-large doc-delete"
                                                        id="{{$Document->id}}">@lang('admin.delete')</button>
                                                <form action="{{ route('admin.provider.document.destroy', [$Provider->id, $Document->id]) }}"
                                                      method="POST" id="form_delete_{{$Document->id}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                </form>
                                            @endcan
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>@lang('admin.provides.document_type')</th>
                            <th>@lang('admin.status')</th>
                            <th>@lang('admin.action')</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            @endcan
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(".doc-delete").on('click', function () {
            var doc_id = $(this).attr('id');
            $("#form_delete_" + doc_id).submit();
        });
    </script>
@endsection
