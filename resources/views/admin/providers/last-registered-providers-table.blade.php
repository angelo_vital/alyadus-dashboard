<div class="box bg-white">
    <div class="box-block clearfix">
        <h5 class="float-xs-left">@lang('admin.dashboard.last-registered-providers')</h5>
        <div class="float-xs-right">
        </div>
    </div>
    <table class="table mb-md-0">
        <tbody>
        <tr>
            <th>@lang('admin.id')</th>
            <th scope="row">@lang('admin.provides.full_name_short')</th>
            <th>@lang('admin.provides.service_type_short')</th>
        </tr>
        @foreach($last_providers as $index => $provider)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $provider->first_name }} {{ $provider->last_name }}</td>
                @can('provider-documents')
                    <td>
                        @if($provider->active_documents() == $total_documents && $provider->service != null)
                            <a class="btn btn-success btn-block" href="{{route('admin.provider.document.index', $provider->id )}}">Verificado</a>
                        @else
                            <a class="btn btn-danger btn-block label-right" href="{{route('admin.provider.document.index', $provider->id )}}">Pendente <span class="btn-label">{{ $provider->pending_documents() }}</span></a>
                        @endif
                    </td>
                @endcan
                <td>
            </tr>
        @endforeach
        </tbody>
        </tbody>
    </table>
</div>