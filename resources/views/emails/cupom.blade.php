<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
    <title>

    </title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        #outlook a { padding:0; }
        .ReadMsgBody { width:100%; }
        .ExternalClass { width:100%; }
        .ExternalClass * { line-height:100%; }
        body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
        table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
        img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
        p { display:block;margin:13px 0; }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
        @media only screen and (max-width:480px) {
            @-ms-viewport { width:320px; }
            @viewport { width:320px; }
        }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
        .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->

    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700" rel="stylesheet" type="text/css">
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,700);
        @import url(https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700);
        @import url(https://fonts.googleapis.com/css?family=Cabin:400,700);
    </style>
    <!--<![endif]-->



    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 { width:100% !important; max-width: 100%; }
        }
    </style>


    <style type="text/css">


    </style>
    <style type="text/css">.hide_on_mobile { display: none !important;}
        @media only screen and (min-width: 480px) { .hide_on_mobile { display: block !important;} }
        [owa] .mj-column-per-100 {
            width: 100%!important;
        }
        [owa] .mj-column-per-50 {
            width: 50%!important;
        }
        [owa] .mj-column-per-33 {
            width: 33.333333333333336%!important;
        }
        p {
            margin: 0px;
        }</style>

</head>
<body style="background-color:#FFFFFF;">


<div style="background-color:#FFFFFF;">


    <!--[if mso | IE]>
    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->

                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                            <tbody><tr>
                                <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">

                                    <div style="font-family:Roboto, Tahoma, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                        <p style="text-align: center;"><span style="font-size: 18px;"><strong>Promo&#xE7;&#xE3;o UBGo!</strong></span></p>
                                    </div>

                                </td>
                            </tr>

                            </tbody></table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->

                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                            <tbody><tr>
                                <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">

                                    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                        <p style="text-align: center;"><span style="font-size: 12px;">A UBGo estar&#xE1; sorteando um celular e um vale compras no valor de 300 reais!</span></p>
                                        <p style="text-align: center;"><span style="font-size: 12px;">A partir desse momento voce j&#xE1; est&#xE1; participando, guarde esse email contendo o n&#xFA;mero do cupom do sorteio, caso saia o seu n&#xFA;mero voce ser&#xE1; o vencedor da promo&#xE7;&#xE3;o!</span></p>
                                    </div>

                                </td>
                            </tr>

                            </tbody></table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->

                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                            <tbody><tr>
                                <td style="font-size:0px;padding:10px 25px;padding-top:10px;padding-right:10px;word-break:break-word;">

                                    <p style="border-top:solid 1px #000000;font-size:1;margin:0px auto;width:100%;">
                                    </p>

                                    <!--[if mso | IE]>
                                    <table
                                            align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #000000;font-size:1;margin:0px auto;width:565px;" role="presentation" width="565px"
                                    >
                                        <tr>
                                            <td style="height:0;line-height:0;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <![endif]-->


                                </td>
                            </tr>

                            </tbody></table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->

                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                            <tbody><tr>
                                <td align="center" vertical-align="middle" style="font-size:0px;padding:20px 20px 20px 20px;word-break:break-word;">

                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;width:auto;line-height:100%;">
                                        <tbody><tr>
                                            <td align="center" bgcolor="#4A90E2" role="presentation" style="border:0px solid #000;border-radius:none;cursor:auto;mso-padding-alt:21px 62px 21px 62px;background:#4A90E2;" valign="middle">
                                                <a href="#" style="display:inline-block;background:#4A90E2;color:#ffffff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:31px;font-weight:normal;line-height:100%;Margin:0;text-decoration:none;text-transform:none;padding:21px 62px 21px 62px;mso-padding-alt:0px;border-radius:none;">
                                                    {{ $code }}
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody></table>

                                </td>
                            </tr>

                            <tr>
                                <td style="font-size:0px;padding:10px 25px;padding-top:10px;padding-right:10px;word-break:break-word;">

                                    <p style="border-top:solid 1px #000000;font-size:1;margin:0px auto;width:100%;">
                                    </p>

                                    <!--[if mso | IE]>
                                    <table
                                            align="center" border="0" cellpadding="0" cellspacing="0" style="border-top:solid 1px #000000;font-size:1;margin:0px auto;width:565px;" role="presentation" width="565px"
                                    >
                                        <tr>
                                            <td style="height:0;line-height:0;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <![endif]-->


                                </td>
                            </tr>

                            </tbody></table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>

    <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
    >
        <tr>
            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->


    <div style="Margin:0px auto;max-width:600px;">

        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0px;padding:9px 0px 9px 0px;text-align:center;vertical-align:top;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">

                        <tr>

                            <td
                                    class="" style="vertical-align:top;width:600px;"
                            >
                    <![endif]-->

                    <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">

                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">

                            <tbody><tr>
                                <td align="left" style="font-size:0px;padding:15px 15px 15px 15px;word-break:break-word;">

                                    <div style="font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:1.5;text-align:left;color:#000000;">
                                        <p>A UBGo agradece a sua participa&#xE7;&#xE3;o, e n&#xE3;o se esque&#xE7;a! Quanto mais voc&#xEB; viajar, mais chances ter&#xE1; de ganhar!</p>
                                    </div>

                                </td>
                            </tr>

                            </tbody></table>

                    </div>

                    <!--[if mso | IE]>
                    </td>

                    </tr>

                    </table>
                    <![endif]-->
                </td>
            </tr>
            </tbody>
        </table>

    </div>


    <!--[if mso | IE]>
    </td>
    </tr>
    </table>
    <![endif]-->


</div>



</body></html>
