@extends('user.layout.auth')

@section('content')

    <?php $login_user = asset('asset/img/login-user-bg.jpg'); ?>
    <div class="full-page-bg" style="background-image: url({{$login_user}});">
        <div class="log-overlay"></div>
        <div class="full-page-bg-inner">
            <div class="row no-margin">
                <div class="col-md-6 log-left">
                <span class="login-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ config('constants.site_logo', asset('logo-black.png'))}}">
                    </a>
                </span>
                    <h2>Crie sua conta e mova-se em minutos</h2>
                    <p>Bem-vindo(a) ao {{config('constants.site_title','Moob Urban')}}, a maneira mais fácil de se
                        locomover com o toque de um botão.</p>
                </div>
                <div class="col-md-6 log-right">
                    <div class="login-box-outer">
                        <div class="login-box row no-margin">
                            <div class="col-md-12">
                                <a class="log-blk-btn" href="{{url('login')}}">JÁ TEM UMA CONTA?</a>
                                <h3>Criar um Conta</h3>
                            </div>
                            <form role="form" method="POST" action="{{ url('/register') }}">

                                <div id="first_step">
                                    <div class="col-md-4">
                                        <input value="+55" type="text" placeholder="+1" id="country_code"
                                               name="country_code"/>
                                    </div>

                                    <div class="col-md-8">
                                        <input type="text" autofocus id="phone_number" class="form-control"
                                               placeholder="Número celular com DDD" name="phone_number"
                                               value="{{ old('phone_number') }}" data-stripe="number" maxlength="11"
                                               onkeypress="return isNumberKey(event);" required/>
                                    </div>

                                    <div class="col-md-8">
                                        @if ($errors->has('phone_number'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12" style="padding-bottom: 10px;" id="mobile_verfication"></div>
                                    <div class="col-md-12" style="padding-bottom: 10px;">
                                        <input type="button" class="log-teal-btn small verify_btn" onclick="smsLogin();"
                                               value="VERIFICAR NÚMERO"/>
                                    </div>

                                </div>

                                {{ csrf_field() }}

                                <div id="second_step" style="display: none;">

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Nome" name="first_name"
                                               value="{{ old('first_name') }}" data-validation="alphanumeric"
                                               data-validation-allowing=" -"
                                               data-validation-error-msg="O nome só pode conter caracteres alfanuméricos e . - e espaços">

                                        @if ($errors->has('first_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Sobrenome" name="last_name"
                                               value="{{ old('last_name') }}" data-validation="alphanumeric"
                                               data-validation-allowing=" -"
                                               data-validation-error-msg="O sobrenome só pode conter caracteres alfanuméricos e . - e espaços">

                                        @if ($errors->has('last_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <input type="email" class="form-control" name="email" placeholder="E-mail"
                                               value="{{ old('email') }}" data-validation="email" data-validation-error-msg="Digite um email válido">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12">
                                        <label class="checkbox">
                                            <input type="radio" name="gender" value="MALE"
                                                   data-validation="required"
                                                   data-validation-error-msg="Selecione um gênero">
                                            Masculino</label>
                                        <label class="checkbox">
                                            <input type="radio" name="gender" value="FEMALE">
                                            Feminino</label>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12">
                                        <input type="password" class="form-control" name="password" placeholder="Senha"
                                               data-validation="length" data-validation-length="min6"
                                               data-validation-error-msg="Senha não deve ser inferior a 6 caracteres">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="col-md-12">
                                        <input type="password" placeholder="Repita a Senha" class="form-control"
                                               name="password_confirmation" data-validation="confirmation"
                                               data-validation-confirm="password"
                                               data-validation-error-msg="As senhas não correspondem">

                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    @if(config('constants.referral') == 1)
                                        <div class="col-md-12">
                                            <input type="text" placeholder="Código de Referência (Opcional)"
                                                   class="form-control" name="referral_code">

                                            @if ($errors->has('referral_code'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('referral_code') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    @else
                                        <input type="hidden" name="referral_code">
                                    @endif

                                    <div class="col-md-12">
                                        <button class="log-teal-btn" type="submit">CADASTRAR</button>
                                    </div>

                                </div>

                            </form>

                            <div class="col-md-12">
                                <p class="helper">Ou <a href="{{route('login')}}">Entre</a> com sua conta de usuário.
                                </p>
                            </div>

                        </div>


                        <div class="log-copy"><p
                                    class="no-margin">{{ config('constants.site_copyright', '&copy; '.date('Y').' Appoets') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection


        @section('scripts')
            <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
            <script type="text/javascript">
                @if (count($errors) > 0)
                $("#second_step").show();
                @endif
                $.validate({
                    modules: 'security',
                });
                $('.checkbox-inline').on('change', function () {
                    $('.checkbox-inline').not(this).prop('checked', false);
                });

                function isNumberKey(evt) {
                    var edValue = document.getElementById("phone_number");
                    var s = edValue.value;
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        if (s.length >= 10) {
                            smsLogin();
                        }
                    }
                    var charCode = (evt.which) ? evt.which : event.keyCode;
                    if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                        return false;
                    return true;
                }
            </script>
            <script src="https://sdk.accountkit.com/pt_BR/sdk.js"></script>
            <script>
                // initialize Account Kit with CSRF protection
                AccountKit_OnInteractive = function () {
                    AccountKit.init(
                        {
                            appId: {{config('constants.facebook_app_id')}},
                            state: "state",
                            version: "{{config('constants.facebook_app_version')}}",
                            fbAppEventsEnabled: true
                        }
                    );
                };

                // login callback
                function loginCallback(response) {
                    if (response.status === "PARTIALLY_AUTHENTICATED") {
                        var code = response.code;
                        var csrf = response.state;
                        // Send code to server to exchange for access token
                        $('#mobile_verfication').html("<p class='helper'> * Número verificado. </p>");
                        $('#phone_number').attr('readonly', true);
                        $('#country_code').attr('readonly', true);
                        $('#second_step').fadeIn(400);
                        $('.verify_btn').hide();
                        $.post("{{route('account.kit')}}", {code: code}, function (data) {
                            $('#phone_number').val(data.phone.national_number);
                            $('#country_code').val('+' + data.phone.country_prefix);
                        });
                    } else if (response.status === "NOT_AUTHENTICATED") {
                        // handle authentication failure
                        $('#mobile_verfication').html("<p class='helper'> * Falha na autenticação </p>");
                    } else if (response.status === "BAD_PARAMS") {
                        // handle bad parameters
                    }
                }

                // phone form submission handler
                function smsLogin() {
                    var countryCode = document.getElementById("country_code").value;
                    var phoneNumber = document.getElementById("phone_number").value;
                    $.post("{{url('/user/verify-credentials')}}", {
                        _token: '{{csrf_token()}}',
                        mobile: countryCode + phoneNumber
                    }).done(function (data) {


                        $('#mobile_verfication').html("<p class='helper'> Por favor, aguarde... </p>");
                        //$('#phone_number').attr('readonly',true);
                        //$('#country_code').attr('readonly',true);

                        AccountKit.login(
                            'PHONE',
                            {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
                            loginCallback
                        );
                    })
                        .fail(function (xhr, status, error) {
                            $('#mobile_verfication').html("<p class='helper'> " + xhr.responseJSON.message + " </p>");
                        });
                }

            </script>

@endsection
