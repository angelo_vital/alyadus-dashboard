<div class="row">
    <div class="col-md-6 page_info">
        Exibindo {{($pagination->currentPage-1)*$pagination->perPage+1}}
        para {{$pagination->currentPage*$pagination->perPage}}
        de {{$pagination->total}} registros
    </div>
    <div class="col-md-6 pagination_cover">
        {{ $pagination->links}}
    </div>
</div>
